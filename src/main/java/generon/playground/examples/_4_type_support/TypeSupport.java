package generon.playground.examples._4_type_support;

import generon.client.ClientAPI;
import generon.client.TenantClientAPI;
import generon.client.type_support_service.TypeSupportServiceBlockingAPI;
import generon.client.type_support_service.TypeSupportServiceFactory;
import generon.playground.configuration.Example;
import generon.playground.configuration.Utils;
import generon.playground.examples._4_type_support.common.type.AgronaTypesObject;
import generon.playground.examples._4_type_support.common.type.ComplexObject;
import generon.playground.examples._4_type_support.common.type.EnumType;
import generon.playground.examples._4_type_support.common.type.OtherTypesObject;
import generon.playground.examples._4_type_support.common.type.SimpleObject;
import generon.playground.examples._4_type_support.common.type.collection.PrimitiveListObject;
import generon.playground.examples._4_type_support.common.type.collection.PrimitiveMapObject;
import generon.playground.examples._4_type_support.common.type.collection.PrimitiveSetObject;
import generon.playground.examples._4_type_support.common.type.complex_object.ComplexObjectV1;
import generon.playground.examples._4_type_support.common.type.primitive.BoxedPrimitiveArrayObject;
import generon.playground.examples._4_type_support.common.type.primitive.BoxedPrimitiveObject;
import generon.playground.examples._4_type_support.common.type.primitive.PrimitiveArrayObject;
import generon.playground.examples._4_type_support.common.type.primitive.PrimitiveArrayObject2;
import generon.playground.examples._4_type_support.common.type.primitive.PrimitiveArrayObject3;
import generon.playground.examples._4_type_support.common.type.primitive.PrimitiveObject;
import org.agrona.collections.Int2IntHashMap;
import org.agrona.collections.Int2NullableObjectHashMap;
import org.agrona.collections.Int2ObjectHashMap;
import org.agrona.collections.IntArrayList;
import org.agrona.collections.IntHashSet;
import org.agrona.collections.Long2LongHashMap;
import org.agrona.collections.Long2NullableObjectHashMap;
import org.agrona.collections.Long2ObjectHashMap;
import org.agrona.collections.LongArrayList;
import org.agrona.collections.LongHashSet;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class TypeSupport implements Example {

    @Override
    public void run(ClientAPI clientAPI) {
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        TypeSupportServiceFactory typeServiceFactory = tenantAPI.typeSupportServiceFactory();
        TypeSupportServiceBlockingAPI api = typeServiceFactory.blocking();

        primitives(api);
        boxedPrimitives(api);
        primitiveArrays(api);
        primitiveArrays2(api);
        primitiveArrays3(api);
        boxedPrimitiveArrays(api);
        primitiveLists(api);
        primitiveSets(api);
        primitiveMaps(api);
        string(api);
        enumValue(api);
        simpleObject(api);
        simpleObjectMap(api);
        complexObject(api);
        nestedGenericTypeMap(api);
        agronaTypes(api);
        otherTypes(api);
    }

    private static void primitives(TypeSupportServiceBlockingAPI api) {
        PrimitiveObject primitives = api.primitives(true, (byte) 10, (short) 1000, 'a', 5.5f, 100_000, 10.10d, 100000000000000L);
        System.out.println("primitives: " + primitives);
    }

    private static void boxedPrimitives(TypeSupportServiceBlockingAPI api) {
        BoxedPrimitiveObject boxedPrimitives = api.boxedPrimitives(true, (byte) 10, (short) 1000, 'a', 5.5f, 100_000, 10.10d, 100000000000000L);
        BoxedPrimitiveObject boxedPrimitivesNull = api.boxedPrimitives(null, null, null, null, null, null, null, null);

        System.out.println("boxedPrimitives: " + boxedPrimitives);
        System.out.println("boxedPrimitives (null): " + boxedPrimitivesNull);
    }

    private static void primitiveArrays(TypeSupportServiceBlockingAPI api) {
        PrimitiveArrayObject primitiveArrayObject = api.primitiveArrays(
                new boolean[]{true, false, true},
                new byte[]{1, 2, 3},
                new short[]{1001, 1002, 1003},
                new char[]{'a', 'b', 'c'},
                new float[]{1.1f, 2.2f, 3.3f},
                new int[]{100_001, 100_002, 100_003},
                new double[]{111.111d, 222.222d, 333.333d},
                new long[]{1000000000000000001L, 1000000000000000002L, 1000000000000000003L}
        );

        PrimitiveArrayObject primitiveArrayObjectNull = api.primitiveArrays(
                null, null, null, null, null, null, null, null
        );

        System.out.println("primitiveArrays: " + primitiveArrayObject);
        System.out.println("primitiveArrays (null): " + primitiveArrayObjectNull);
    }

    private static void primitiveArrays2(TypeSupportServiceBlockingAPI api) {
        PrimitiveArrayObject2 primitiveArrayObject2 = api.primitiveArrays2(
                new boolean[][]{null, new boolean[0], new boolean[]{true, false}},
                new byte[][]{null, new byte[0], new byte[]{1, 2}},
                new short[][]{null, new short[0], new short[]{1001, 1002}},
                new char[][]{null, new char[0], new char[]{'a', 'b'}},
                new float[][]{null, new float[0], new float[]{1.1f, 2.2f}},
                new int[][]{null, new int[0], new int[]{100_001, 100_002}},
                new double[][]{null, new double[0], new double[]{111.111d, 222.222d}},
                new long[][]{null, new long[0], new long[]{1000000000000000001L, 1000000000000000002L}}
        );

        PrimitiveArrayObject2 primitiveArrayObject2Null = api.primitiveArrays2(
                null, null, null, null, null, null, null, null
        );

        System.out.println("primitiveArrays2: " + primitiveArrayObject2);
        System.out.println("primitiveArrays2 (null): " + primitiveArrayObject2Null);
    }

    private static void primitiveArrays3(TypeSupportServiceBlockingAPI api) {
        PrimitiveArrayObject3 primitiveArrayObject3 = api.primitiveArrays3(
                new boolean[][][]{null, new boolean[0][], new boolean[][]{new boolean[]{true, false}, new boolean[]{false, true}}},
                new byte[][][]{null, new byte[0][], new byte[][]{new byte[]{1, 2}, new byte[]{3, 4}}},
                new short[][][]{null, new short[0][], new short[][]{new short[]{1001, 1002}, new short[]{1003, 1004}}},
                new char[][][]{null, new char[0][], new char[][]{new char[]{'a', 'b'}, new char[]{'c', 'd'}}},
                new float[][][]{null, new float[0][], new float[][]{new float[]{1.1f, 2.2f}, new float[]{3.3f, 4.4f}}},
                new int[][][]{null, new int[0][], new int[][]{new int[]{100_001, 100_002}, new int[]{100_003, 100_004}}},
                new double[][][]{null, new double[0][], new double[][]{new double[]{111.111d, 222.222d}, new double[]{333.333d, 444.444d}}},
                new long[][][]{null, new long[0][], new long[][]{new long[]{1000000000000000001L, 1000000000000000002L}, new long[]{1000000000000000003L, 1000000000000000004L}}}
        );

        PrimitiveArrayObject3 primitiveArrayObject2Null = api.primitiveArrays3(
                null, null, null, null, null, null, null, null
        );

        System.out.println("primitiveArrays3: " + primitiveArrayObject3);
        System.out.println("primitiveArrays3 (null): " + primitiveArrayObject2Null);
    }

    private static void boxedPrimitiveArrays(TypeSupportServiceBlockingAPI api) {
        BoxedPrimitiveArrayObject boxedPrimitiveArrayObject = api.boxedPrimitiveArrays(
                new Boolean[]{true, false, null},
                new Byte[]{1, 2, null},
                new Short[]{1001, 1002, null},
                new Character[]{'a', 'b', null},
                new Float[]{1.1f, 2.2f, null},
                new Integer[]{100_001, 100_002, null},
                new Double[]{111.111d, 222.222d, null},
                new Long[]{1000000000000000001L, 1000000000000000002L, null}
        );

        BoxedPrimitiveArrayObject boxedPrimitiveArrayObjectNull = api.boxedPrimitiveArrays(
                null, null, null, null, null, null, null, null
        );

        System.out.println("boxedPrimitiveArrays: " + boxedPrimitiveArrayObject);
        System.out.println("boxedPrimitiveArrays (null): " + boxedPrimitiveArrayObjectNull);
    }

    private static void primitiveLists(TypeSupportServiceBlockingAPI api) {
        PrimitiveListObject primitiveListObject = api.primitiveLists(
                Arrays.asList(true, false, null),
                Arrays.asList((byte) 1, (byte) 2, null),
                Arrays.asList((short) 1001, (short) 1002, null),
                Arrays.asList('a', 'b', null),
                Arrays.asList(1.1f, 2.2f, null),
                Arrays.asList(100_001, 100_002, null),
                Arrays.asList(111.111d, 222.222d, null),
                Arrays.asList(1000000000000000001L, 1000000000000000002L, null)
        );

        PrimitiveListObject primitiveListObjectNull = api.primitiveLists(
                null, null, null, null, null, null, null, null
        );

        System.out.println("primitiveLists: " + primitiveListObject);
        System.out.println("primitiveLists (null): " + primitiveListObjectNull);
    }

    private static void primitiveSets(TypeSupportServiceBlockingAPI api) {
        PrimitiveSetObject primitiveSetObject = api.primitiveSets(
                new HashSet<>(Arrays.asList(true, false, null)),
                new HashSet<>(Arrays.asList((byte) 1, (byte) 2, null)),
                new HashSet<>(Arrays.asList((short) 1001, (short) 1002, null)),
                new HashSet<>(Arrays.asList('a', 'b', null)),
                new HashSet<>(Arrays.asList(1.1f, 2.2f, null)),
                new HashSet<>(Arrays.asList(100_001, 100_002, null)),
                new HashSet<>(Arrays.asList(111.111d, 222.222d, null)),
                new HashSet<>(Arrays.asList(1000000000000000001L, 1000000000000000002L, null))
        );

        PrimitiveSetObject primitiveSetObjectNull = api.primitiveSets(
                null, null, null, null, null, null, null, null
        );

        System.out.println("primitiveSets: " + primitiveSetObject);
        System.out.println("primitiveSets (null): " + primitiveSetObjectNull);
    }

    private static void primitiveMaps(TypeSupportServiceBlockingAPI api) {
        Map<Integer, Boolean> booleans = new HashMap<>();
        booleans.put(1, true);
        booleans.put(2, false);
        booleans.put(3, null);

        Map<Integer, Byte> bytes = new HashMap<>();
        bytes.put(1, (byte) 1);
        bytes.put(2, (byte) 2);
        bytes.put(3, null);

        Map<Integer, Short> shorts = new HashMap<>();
        shorts.put(1, (short) 1001);
        shorts.put(2, (short) 1002);
        shorts.put(3, null);

        Map<Integer, Character> chars = new HashMap<>();
        chars.put(1, 'a');
        chars.put(2, 'b');
        chars.put(3, null);

        Map<Integer, Float> floats = new HashMap<>();
        floats.put(1, 1.1f);
        floats.put(2, 2.2f);
        floats.put(3, null);

        Map<Integer, Integer> ints = new HashMap<>();
        ints.put(1, 100_001);
        ints.put(2, 100_002);
        ints.put(3, null);

        Map<Integer, Double> doubles = new HashMap<>();
        doubles.put(1, 111.111d);
        doubles.put(2, 222.222d);
        doubles.put(3, null);

        Map<Integer, Long> longs = new HashMap<>();
        longs.put(1, 1000000000000000001L);
        longs.put(2, 1000000000000000002L);
        longs.put(3, null);

        PrimitiveMapObject primitiveMapObject = api.primitiveMaps(booleans, bytes, shorts, chars, floats, ints, doubles, longs);
        PrimitiveMapObject primitiveMapObjectNull = api.primitiveMaps(null, null, null, null, null, null, null, null);

        System.out.println("primitiveMaps: " + primitiveMapObject);
        System.out.println("primitiveMaps (null): " + primitiveMapObjectNull);
    }

    private static void string(TypeSupportServiceBlockingAPI api) {
        String string = api.string("Hello, Generon");
        String stringNull = api.string(null);

        System.out.println("string: " + string);
        System.out.println("string (null): " + stringNull);
    }

    private static void enumValue(TypeSupportServiceBlockingAPI api) {
        EnumType enumValue = api.enumValue(EnumType.VALUE_1);
        EnumType enumValueNull = api.enumValue(null);

        System.out.println("enumValue: " + enumValue);
        System.out.println("enumValue (null): " + enumValueNull);
    }

    private static void simpleObject(TypeSupportServiceBlockingAPI api) {
        SimpleObject simpleObject = api.simpleObject(new SimpleObject(10_000, new byte[]{1, 2}));
        SimpleObject simpleObjectNull = api.simpleObject(null);

        System.out.println("simpleObject: " + simpleObject);
        System.out.println("simpleObject (null): " + simpleObjectNull);
    }

    private static void simpleObjectMap(TypeSupportServiceBlockingAPI api) {
        Map<String, SimpleObject> request = new HashMap<>();
        request.put("first", new SimpleObject(1, null));
        request.put("second", new SimpleObject(2, new byte[0]));
        request.put("third", new SimpleObject(3, null));

        System.out.println("simpleObjectMap: " + api.simpleObjectMap(request));
    }

    private static void complexObject(TypeSupportServiceBlockingAPI api) {
        ComplexObjectV1 request = new ComplexObjectV1();
        request.string = "string value";
        request.enumValue = EnumType.VALUE_2;
        request.subObjects = List.of(new SimpleObject(0, null));

        System.out.println("complexObject: " + api.complexObject(request));
    }

    private static void nestedGenericTypeMap(TypeSupportServiceBlockingAPI api) {
        Map<String, Map<Integer, List<ComplexObject[]>>> nestedGenericTypeMap = api.nestedGenericTypeMap(4);
        System.out.println("nestedGenericTypeMap: " + Utils.deepToString(nestedGenericTypeMap));
    }

    private static void agronaTypes(TypeSupportServiceBlockingAPI api) {
        IntArrayList intArrayList = new IntArrayList();
        IntHashSet intHashSet = new IntHashSet();
        Int2IntHashMap int2IntHashMap = new Int2IntHashMap(0, 0.65F, Integer.MIN_VALUE);
        Int2ObjectHashMap<String> int2ObjectHashMap = new Int2ObjectHashMap<>();
        Int2NullableObjectHashMap<String> int2NullableObjectHashMap = new Int2NullableObjectHashMap<>();

        LongArrayList longArrayList = new LongArrayList();
        LongHashSet longHashSet = new LongHashSet();
        Long2LongHashMap long2LongHashMap = new Long2LongHashMap(0, 0.65F, Long.MIN_VALUE);
        Long2ObjectHashMap<String> long2ObjectHashMap = new Long2ObjectHashMap<>();
        Long2NullableObjectHashMap<String> long2NullableObjectHashMap = new Long2NullableObjectHashMap<>();

        intArrayList.addInt(Integer.MIN_VALUE);
        intHashSet.add(Integer.MIN_VALUE);
        int2IntHashMap.put(1, 100);
        int2ObjectHashMap.put(1, "Int2ObjectHashMap");
        int2NullableObjectHashMap.put(1, null);
        int2NullableObjectHashMap.put(2, "Not null");

        longArrayList.addLong(Long.MIN_VALUE);
        longHashSet.add(Long.MIN_VALUE);
        long2LongHashMap.put(1L, 100L);
        long2ObjectHashMap.put(1L, "Long2ObjectHashMap");
        long2NullableObjectHashMap.put(1L, null);
        long2NullableObjectHashMap.put(2L, "Not null");

        AgronaTypesObject agronaTypes = api.agronaTypes(
                intArrayList, intHashSet, int2IntHashMap, int2ObjectHashMap, int2NullableObjectHashMap,
                longArrayList, longHashSet, long2LongHashMap, long2ObjectHashMap, long2NullableObjectHashMap
        );
        AgronaTypesObject agronaTypesNull = api.agronaTypes(null, null, null, null, null, null, null, null, null, null);

        System.out.println("agronaTypes: " + agronaTypes);
        System.out.println("agronaTypes (null): " + agronaTypesNull);
    }

    private static void otherTypes(TypeSupportServiceBlockingAPI api) {
        OtherTypesObject otherTypes = api.otherTypes(new BigDecimal("123.456789"), new BigInteger("12345678900987654321"));
        OtherTypesObject otherTypesNull = api.otherTypes(null, null);

        System.out.println("otherTypes: " + otherTypes);
        System.out.println("otherTypes (null): " + otherTypesNull);
    }

}


