package generon.playground.examples._13_migration_management.common.event_producer;

import generon.core.annotation.scan.event.EventMethod;
import generon.core.annotation.scan.event.EventProducer;
import generon.playground.configuration.Constants;

@EventProducer(value = Constants.Id.EventProducer.MIGRATION, streamId = Constants.EventStreamId.MIGRATION)
public interface MigrationEventProducer {

    @EventMethod(1)
    void eventV1(int id);

    @EventMethod(value = 1, version = 2)
    void eventV2(int id, boolean flag);

}
