### Supported types

1. Primitives (Including boxed)
2. Arrays (Including multidimensional)
3. POJOs with nested objects. (Classes annotated with @Serializable)
4. JDK types: BigDecimal, BigInteger
5. Collections from JDK: Collection, List, Set, Map. All implementations are supported automatically - ArrayList,
   HashMap etc.
6. Collections from Aeron Agrona:
   IntArrayList, IntHashSet, Int2IntHashMap, Int2ObjectHashMap<?>, Int2NullableObjectHashMap<?>,
   LongArrayList, LongHashSet, Long2LongHashMap, Long2ObjectHashMap<?>, Long2NullableObjectHashMap<?>

Protocol supports recursive generic types like Map<String, List<MyObject[]>[]> and type can be as complex as needed.
POJO can not have circular dependencies with nested field types. When User has field with Account type and Account has
field with User type or when there are in-between types it will not pass build validation. There will be recursion on
the code generation level which makes it impossible.

Other types can easily be integrated with Generon protocol. It requires implementation of writer and reader and is
covered in the `CustomType` example.

### When Generon Protocol is used ?

1. API request/response communication.
2. @SnapshotField, when data is applied to or taken from snapshot.
3. Event management, when event is serialized to the stream on server side and deserialize when consumer reads it on
   client side.

All types including custom ones will automatically be supported in all these scenarios including collection/map generics
and nested @Serializable object types.