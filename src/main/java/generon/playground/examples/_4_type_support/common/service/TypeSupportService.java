package generon.playground.examples._4_type_support.common.service;

import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;
import generon.playground.examples._4_type_support.common.type.AgronaTypesObject;
import generon.playground.examples._4_type_support.common.type.ComplexObject;
import generon.playground.examples._4_type_support.common.type.EnumType;
import generon.playground.examples._4_type_support.common.type.OtherTypesObject;
import generon.playground.examples._4_type_support.common.type.SimpleObject;
import generon.playground.examples._4_type_support.common.type.collection.PrimitiveListObject;
import generon.playground.examples._4_type_support.common.type.collection.PrimitiveMapObject;
import generon.playground.examples._4_type_support.common.type.collection.PrimitiveSetObject;
import generon.playground.examples._4_type_support.common.type.primitive.BoxedPrimitiveArrayObject;
import generon.playground.examples._4_type_support.common.type.primitive.BoxedPrimitiveObject;
import generon.playground.examples._4_type_support.common.type.primitive.PrimitiveArrayObject;
import generon.playground.examples._4_type_support.common.type.primitive.PrimitiveArrayObject2;
import generon.playground.examples._4_type_support.common.type.primitive.PrimitiveArrayObject3;
import generon.playground.examples._4_type_support.common.type.primitive.PrimitiveObject;
import org.agrona.collections.Int2IntHashMap;
import org.agrona.collections.Int2NullableObjectHashMap;
import org.agrona.collections.Int2ObjectHashMap;
import org.agrona.collections.IntArrayList;
import org.agrona.collections.IntHashSet;
import org.agrona.collections.Long2LongHashMap;
import org.agrona.collections.Long2NullableObjectHashMap;
import org.agrona.collections.Long2ObjectHashMap;
import org.agrona.collections.LongArrayList;
import org.agrona.collections.LongHashSet;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service(Constants.Id.Service.TYPE_SUPPORT)
public interface TypeSupportService {

    // Primitive

    @ServiceMethod(1)
    PrimitiveObject primitives(boolean booleanValue, byte byteValue, short shortValue, char charValue, float floatValue, int intValue, double doubleValue, long longValue);

    @ServiceMethod(2)
    BoxedPrimitiveObject boxedPrimitives(Boolean booleanValue, Byte byteValue, Short shortValue, Character charValue, Float floatValue, Integer intValue, Double doubleValue, Long longValue);

    @ServiceMethod(3)
    PrimitiveArrayObject primitiveArrays(boolean[] booleans, byte[] bytes, short[] shorts, char[] chars, float[] floats, int[] ints, double[] doubles, long[] longs);

    @ServiceMethod(4)
    PrimitiveArrayObject2 primitiveArrays2(boolean[][] booleans, byte[][] bytes, short[][] shorts, char[][] chars, float[][] floats, int[][] ints, double[][] doubles, long[][] longs);

    @ServiceMethod(5)
    PrimitiveArrayObject3 primitiveArrays3(boolean[][][] booleans, byte[][][] bytes, short[][][] shorts, char[][][] chars, float[][][] floats, int[][][] ints, double[][][] doubles, long[][][] longs);

    @ServiceMethod(6)
    BoxedPrimitiveArrayObject boxedPrimitiveArrays(Boolean[] booleans, Byte[] bytes, Short[] shorts, Character[] chars, Float[] floats, Integer[] ints, Double[] doubles, Long[] longs);

    @ServiceMethod(7)
    PrimitiveListObject primitiveLists(List<Boolean> booleans, List<Byte> bytes, List<Short> shorts, List<Character> chars, List<Float> floats, List<Integer> ints, List<Double> doubles, List<Long> longs);

    @ServiceMethod(8)
    PrimitiveSetObject primitiveSets(Set<Boolean> booleans, Set<Byte> bytes, Set<Short> shorts, Set<Character> chars, Set<Float> floats, Set<Integer> ints, Set<Double> doubles, Set<Long> longs);

    @ServiceMethod(9)
    PrimitiveMapObject primitiveMaps(Map<Integer, Boolean> booleans, Map<Integer, Byte> bytes, Map<Integer, Short> shorts, Map<Integer, Character> chars, Map<Integer, Float> floats, Map<Integer, Integer> ints, Map<Integer, Double> doubles, Map<Integer, Long> longs);

    // Object

    @ServiceMethod(10)
    String string(String string);

    @ServiceMethod(11)
    EnumType enumValue(EnumType enumValue);

    @ServiceMethod(12)
    SimpleObject simpleObject(SimpleObject object);

    @ServiceMethod(13)
    Map<String, SimpleObject> simpleObjectMap(Map<String, SimpleObject> map);

    @ServiceMethod(14)
    ComplexObject complexObject(ComplexObject object);

    // Nested generic types

    @ServiceMethod(15)
    Map<String, Map<Integer, List<ComplexObject[]>>> nestedGenericTypeMap(int entryAmount);

    // Agrona types

    @ServiceMethod(16)
    AgronaTypesObject agronaTypes(
            IntArrayList intArrayList, IntHashSet intHashSet, Int2IntHashMap int2IntHashMap, Int2ObjectHashMap<String> int2ObjectHashMap, Int2NullableObjectHashMap<String> int2NullableObjectHashMap,
            LongArrayList longArrayList, LongHashSet longHashSet, Long2LongHashMap long2LongHashMap, Long2ObjectHashMap<String> long2ObjectHashMap, Long2NullableObjectHashMap<String> long2NullableObjectHashMap
    );

    @ServiceMethod(17)
    OtherTypesObject otherTypes(BigDecimal bigDecimal, BigInteger bigInteger);

}
