package generon.playground.examples._11_timer_management;

import generon.client.ClientAPI;
import generon.client.TenantClientAPI;
import generon.client.time_management_service.TimeManagementServiceBlockingAPI;
import generon.client.time_management_service.TimeManagementServiceFactory;
import generon.playground.configuration.Example;
import generon.playground.configuration.Utils;

public class TimerManagement implements Example {

    @Override
    public void run(ClientAPI clientAPI) {
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        TimeManagementServiceFactory timeManagementServiceFactory = tenantAPI.timeManagementServiceFactory();
        TimeManagementServiceBlockingAPI api = timeManagementServiceFactory.blocking();

        long timerId1 = api.schedule(1, 10L, "Timer 1");
        long timerId2 = api.schedule(2, 10L, "Timer 2");

        Utils.sleep(50);

        long timerId3 = api.scheduleInfinite(50L, "Infinite Timer");

        Utils.sleep(150); // Wait for some events
        System.out.println("Infinite timer is cancelled: " + api.cancel(timerId3));
        Utils.sleep(75); // No more event should be received

        System.out.println("Total triggered: " + api.totalTriggered());
    }

}
