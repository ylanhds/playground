package generon.playground.examples._5_custom_type;

import generon.client.ClientAPI;
import generon.client.TenantClientAPI;
import generon.client.custom_type_service.CustomTypeServiceBlockingAPI;
import generon.client.custom_type_service.CustomTypeServiceFactory;
import generon.playground.configuration.Example;
import generon.playground.examples._5_custom_type.common.type.buffer_object.InstantBO;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class CustomType implements Example {

    private static final InstantBO INSTANT_BO = new InstantBO();

    @Override
    public void run(ClientAPI clientAPI) {
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        CustomTypeServiceFactory customTypeServiceFactory = tenantAPI.customTypeServiceFactory();
        CustomTypeServiceBlockingAPI api = customTypeServiceFactory.blocking();

        instantTC(api);
        instantBO_1(api);
        instantBO_2(api);
        instantBO_3(api);
        instantBO_4(api);
        recursiveTypes(api);
    }

    private static void instantTC(CustomTypeServiceBlockingAPI api) {
        System.out.println("instantTC: " + api.instantTC(Instant.now()));
        System.out.println("instantTC (null): " + api.instantTC(null));
    }

    private static void instantBO_1(CustomTypeServiceBlockingAPI api) {
        INSTANT_BO.writer().wrap(Instant.now());
        InstantBO response = api.instantBO_1(INSTANT_BO);
        System.out.println("instantBO_1: " + response.reader().asInstant());

        INSTANT_BO.writer().wrapAsNull();
        InstantBO response2 = api.instantBO_1(INSTANT_BO);
        System.out.println("instantBO_1 (null): " + response2.reader().asInstant());
    }

    private static void instantBO_2(CustomTypeServiceBlockingAPI api) {
        InstantBO response = api.instantBO_2(Instant.now().toEpochMilli());
        System.out.println("instantBO_2: " + response.reader().asInstant());

        InstantBO response2 = api.instantBO_2(null);
        System.out.println("instantBO_2 (null): " + response2.reader().asInstant());
    }

    private static void instantBO_3(CustomTypeServiceBlockingAPI api) {
        INSTANT_BO.writer().wrap(Instant.now());
        Long response = api.instantBO_3(INSTANT_BO);
        System.out.println("instantBO_3: " + response);

        INSTANT_BO.writer().wrapAsNull();
        Long response2 = api.instantBO_3(INSTANT_BO);
        System.out.println("instantBO_3 (null): " + response2);
    }

    private static void instantBO_4(CustomTypeServiceBlockingAPI api) {
        INSTANT_BO.writer().wrap(Instant.now());
        Instant response = api.instantBO_4(INSTANT_BO);
        System.out.println("instantBO_4: " + response);

        INSTANT_BO.writer().wrapAsNull();
        Instant response2 = api.instantBO_4(INSTANT_BO);
        System.out.println("instantBO_4 (null): " + response2);
    }

    private static void recursiveTypes(CustomTypeServiceBlockingAPI api) {
        InstantBO instantBO1 = new InstantBO();
        InstantBO instantBO2 = new InstantBO();
        InstantBO instantBO3 = new InstantBO();

        instantBO1.writer().wrapAsNull();
        instantBO2.writer().wrap(Instant.now());
        instantBO3.writer().wrap(Instant.now().plus(1, ChronoUnit.HOURS));

        List<Instant> response = api.genericTypes(List.of(instantBO1, instantBO2, instantBO3));
        System.out.println("recursiveTypes: " + response);
    }

}
