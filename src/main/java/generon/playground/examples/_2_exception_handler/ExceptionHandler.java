package generon.playground.examples._2_exception_handler;

import generon.client.ClientAPI;
import generon.client.TenantClientAPI;
import generon.client.exception_handler_service.ExceptionHandlerServiceAsyncAPI;
import generon.client.exception_handler_service.ExceptionHandlerServiceBlockingAPI;
import generon.client.exception_handler_service.ExceptionHandlerServiceFactory;
import generon.integration.objects.GeneronException;
import generon.integration.objects.GeneronProcessingException;
import generon.integration.objects.GeneronTimeoutException;
import generon.playground.configuration.ConfigurableExample;
import generon.playground.configuration.ConsoleColor;
import generon.playground.configuration.TestConfigurator;
import generon.playground.configuration.Utils;

public class ExceptionHandler implements ConfigurableExample {

    @Override
    public void run(ClientAPI clientAPI) {
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        ExceptionHandlerServiceFactory exceptionHandlerServiceFactory = tenantAPI.exceptionHandlerServiceFactory();

        // Async
        ConsoleColor.WHITE_BOLD.print("Async:");

        ExceptionHandlerServiceAsyncAPI asyncAPI = exceptionHandlerServiceFactory.async();

        asyncAPI.testException(1, System.out::println, System.out::println);
        asyncAPI.testException(2, System.out::println, System.out::println);
        asyncAPI.testException(3, System.out::println, System.out::println);

        Utils.sleep(200L); // Await server sleep is passed after action 3

        // Blocking
        ConsoleColor.WHITE_BOLD.print("Blocking:");

        ExceptionHandlerServiceBlockingAPI blockingAPI = exceptionHandlerServiceFactory.blocking();

        try {
            blockingAPI.testException(1);
        } catch (GeneronProcessingException e) {
            System.out.println(e.toErrorMessage());
        }

        try {
            blockingAPI.testException(2);
        } catch (GeneronException e) {
            System.out.println(e.toErrorMessage());
        }

        try {
            blockingAPI.testException(3);
        } catch (GeneronTimeoutException e) {
            System.out.println(e.toErrorMessage());
        }
    }

    /**
     * Disable error handler. It is used to remove expected exception stack trace from the console.
     * Do not do this in real life :)
     * <p>
     * Also, it reduces timeouts to take less time for the example to run. Defaults are 5000L and 100 (ms)
     */
    @Override
    public TestConfigurator testConfigurator() {
        return new TestConfigurator()
                .disableErrorLogs()
                .requestTimeout(150L, 5);
    }
}
