package generon.playground.examples._6_serializable_object.server.service;

import generon.core.annotation.SnapshotField;
import generon.core.annotation.scan.serializable.migration.object.ObjectVersion;
import generon.playground.examples._6_serializable_object.common.service.SerializableObjectService;
import generon.playground.examples._6_serializable_object.common.type.Address;
import generon.playground.examples._6_serializable_object.common.type.User;

import java.util.HashMap;
import java.util.Map;

public class SerializableObjectServiceImpl implements SerializableObjectService {

    @SnapshotField(1)
    @ObjectVersion(value = 2, type = User.class)
    private final Map<Long, User> users = new HashMap<>();

    @SnapshotField(2)
    private long userIdCounter = 1;

    @Override
    public User createUserV1(String name) {
        User user = new User();
        user.version = 1;
        user.id = userIdCounter++;
        user.name = name;

        return storeUser(user);
    }

    @Override
    public User createUserV2(String name, String fullAddress) {
        User user = new User();
        user.version = 2;
        user.id = userIdCounter++;
        user.name = name;
        user.fullAddress = fullAddress;

        return storeUser(user);
    }

    @Override
    public User createUserV3(String name, Address address) {
        User user = new User();
        user.version = 3;
        user.id = userIdCounter++;
        user.name = name;
        user.address = address; // Address.version is always 1

        return storeUser(user);
    }

    @Override
    public User createUserV4(String name, Address address) {
        User user = new User();
        user.version = 4;
        user.id = userIdCounter++;
        user.name = name;
        user.address = address; // Address.version is always 2

        return storeUser(user);
    }

    @Override
    public User createUserV5(String name, Address address) {
        User user = new User();
        user.version = 5;
        user.id = userIdCounter++;
        user.name = name;
        user.address = address; // Address.version can be different

        return storeUser(user);
    }

//    ---

    @Override
    public User createUserV1(User user) {
        return storeUser(user); // User.version is always 1
    }

    @Override
    public User createUserV2(User user) {
        return storeUser(user); // User.version is always 2
    }

    @Override
    public User createUserV3(User user) {
        return storeUser(user); // User.version is always 3, Address.version is always 1
    }

    @Override
    public User createUserV4(User user) {
        return storeUser(user); // User.version is always 4, Address.version is always 1
    }

    @Override
    public User createUserV5(User user) {
        return storeUser(user); // User.version is always 4, Address.version can be different
    }

    @Override
    public User createUser(User user) {
        return storeUser(user); // User.version & Address.version can be different
    }

    private User storeUser(User user) {
        users.put(user.id, user);
        return user;
    }

}
