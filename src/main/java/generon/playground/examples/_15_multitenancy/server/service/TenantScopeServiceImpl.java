package generon.playground.examples._15_multitenancy.server.service;

import generon.core.annotation.BeforeTenantRemoved;
import generon.core.annotation.Inject;
import generon.core.annotation.OnTenantAdded;
import generon.core.annotation.SnapshotField;
import generon.core.annotation.Value;
import generon.integration.cluster.internal.component.TenantMetadataComponent;
import generon.playground.examples._15_multitenancy.common.service.GlobalScopeService;
import generon.playground.examples._15_multitenancy.common.service.TenantScopeService;
import generon.playground.examples._15_multitenancy.common.value_object.TenantValueObject;
import org.agrona.collections.IntArrayList;

public class TenantScopeServiceImpl implements TenantScopeService {

    @Inject
    private TenantMetadataComponent tenantMetadataComponent;

    @Inject
    private GlobalScopeService globalScopeService;

    @Inject
    private TenantValueObject valueObject;

    @Value("tenant_name")
    private String tenantName;

    @SnapshotField(1)
    private final IntArrayList numbers = new IntArrayList();

    @Override
    public void addNumber(int number) {
        numbers.add(number);
    }

    @Override
    public IntArrayList getNumbers() {
        return numbers;
    }

    /**
     * Tenant scope hooks. Triggered in scope of tenant itself. Method should not have any parameters.
     */
    @OnTenantAdded
    private void onTenantAdded() {
        globalScopeService.addFromTenantScope(tenantMetadataComponent.tenantId());
    }

    /**
     * Tenant scope hooks. Triggered in scope of tenant itself. Method should not have any parameters.
     */
    @BeforeTenantRemoved
    private void beforeTenantRemoved() {
        globalScopeService.removeFromTenantScope(tenantMetadataComponent.tenantId());
    }

}
