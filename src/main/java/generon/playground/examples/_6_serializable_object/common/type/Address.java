package generon.playground.examples._6_serializable_object.common.type;

import generon.core.annotation.scan.serializable.Serializable;
import generon.core.annotation.scan.serializable.Version;
import generon.core.annotation.scan.serializable.migration.field.FromVersion;
import generon.core.annotation.scan.serializable.migration.field.RemovedFromVersion;

@Serializable
public class Address {

    @Version
    public byte version;

    @FromVersion(1) // Can be removed because 1 is default
    @RemovedFromVersion(2)
    public String fullAddress;

    @FromVersion(2)
    public String country;

    @FromVersion(2)
    public String state;

    @FromVersion(2)
    public String city;

    @FromVersion(2)
    public String street;

    @FromVersion(2)
    public String postalCode;

    @FromVersion(2)
    public String buildingNumber;

    @Override
    public String toString() {
        return "Address{" +
                "version=" + version +
                ", fullAddress='" + fullAddress + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", buildingNumber='" + buildingNumber + '\'' +
                '}';
    }
}
