package generon.playground.examples._8_dependency_injection.server.component;

import generon.core.annotation.SnapshotField;
import generon.core.annotation.scan.component.Component;
import generon.playground.configuration.Constants;

@Component(Constants.Id.Component.ENTITY_ID_COUNTER)
public class EntityIdCounter {

    @SnapshotField(1)
    private int entityId;

    public int nextEntityId() {
        return ++entityId;
    }

}
