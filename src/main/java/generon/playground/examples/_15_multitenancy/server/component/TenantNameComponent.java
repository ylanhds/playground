package generon.playground.examples._15_multitenancy.server.component;

import generon.core.annotation.Init;
import generon.core.annotation.Inject;
import generon.core.annotation.scan.component.Component;
import generon.integration.cluster.internal.component.TenantMetadataComponent;

@Component
public class TenantNameComponent {

    @Inject
    private TenantMetadataComponent tenantMetadataComponent;

    private String tenantName;

    @Init
    private void init() {
        tenantName = "TenantName " + tenantMetadataComponent.tenantId();
    }

    public String tenantName() {
        return tenantName;
    }
}
