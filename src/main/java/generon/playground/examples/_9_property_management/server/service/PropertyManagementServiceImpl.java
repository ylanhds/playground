package generon.playground.examples._9_property_management.server.service;

import generon.core.annotation.Init;
import generon.core.annotation.Inject;
import generon.core.annotation.Value;
import generon.playground.examples._9_property_management.common.service.PropertyManagementService;
import generon.playground.examples._9_property_management.common.type.PropertyValueContainer;
import generon.playground.examples._9_property_management.common.type.value_object.DynamicValueObject;
import generon.value_object.PropertyConstants;

public class PropertyManagementServiceImpl implements PropertyManagementService {

    @Inject
    private DynamicValueObject dynamicValueObject;

    @Value("app_name")
    private String appName;

    @Value("loaded.property")
    private String loadedProperty;

    @Value("server.url")
    private String url;

    @Value(value = "profile-value", defaultValue = "Profile is not enabled")
    private String profileValue;

    private PropertyValueContainer dynamicPropertyContainer;
    private PropertyValueContainer staticPropertyContainer;

    @Init
    private void init() {
        dynamicPropertyContainer = PropertyValueContainer.of(dynamicValueObject);
        staticPropertyContainer = PropertyValueContainer.of(PropertyConstants.getInstance());
    }

    @Override
    public PropertyValueContainer dynamicProperties() {
        return dynamicPropertyContainer;
    }

    @Override
    public PropertyValueContainer staticProperties() {
        return staticPropertyContainer;
    }

    @Override
    public String appName() {
        return appName;
    }

    @Override
    public String loadedProperty() {
        return loadedProperty;
    }

    @Override
    public String url() {
        return url;
    }

    @Override
    public String profileValue() {
        return profileValue;
    }

}
