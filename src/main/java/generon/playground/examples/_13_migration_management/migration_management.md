### General overview

During development process, it is almost impossible to predict all in advance, which usual thing. After some time we may
need to update data structures and algorithms. In addition to api and object versioning we may also need to version
logic itself. For example, we have `@SnapshotField` with `List<User>` type. Let's assume that initially we did not think
about getting user by id from the collection, but code is already in the production. Sure, we can find user with `O(N)`,
but we want `O(1)` and it requires changing type to something like `Long2ObjectHashMap<User>`. To make it work we need
to start using new data structure and stop using previous one at some point of time. Same approach we can use to version
events. For example, we have `userCreatedV1` event. But we did extension, and now we need to trigger `userCreatedV2`
instead. We need to switch old event with new one, while being able to do replays and keep historical logic.

### Problem

In usual systems it is enough just to deploy new code version because no-one cares that much about data version,
history, replays etc. And it is fine. We usually use some tools like Liquibase to track schema changes. Also, JSON is
commonly used as it is fully compatible with older versions. When it comes to low-level system we need to consider lots
of small details. We can not easily replace these tools and keep performance at high level.
<br>
<br>
When we have new event version we can not just deploy code which will trigger `V2` instead of `V1` because node after
start can have some commands to replay which are actually old and logic should trigger `V1` to behave deterministically.
To solve this issue we could just trigger both versions at the same time to support all at once, but it will make
performance worse due to additional load. System with this approach is not scalable but at least it does
the job.
<br>
<br>
For `List<User>` case, we can not just deploy code with new data structure instead because of snapshots. Data will still
be stored in snapshot as structure - `List<String>` and will not be deserialized correctly. Replay from beginning
without snapshots will work fine in this case because code will just populate data using new structure as it was there
from the beginning. We can use this trick to do a migration. Another common approach is to stop all services which can
write to the cluster, wait few seconds to ensure all commands are processed, migrate data and make a snapshot with new
structure. Or make a snapshot, deploy new code and migrate data structures at the startup. Both approaches require some
time to do a migration, meaning it requires to stop cluster completely for this maintenance period which can take
minutes in best case. This is fine for some cases, but it does not solve a problem, which is code updates on the fly
without stopping the cluster, It is called `zero-downtime deployment`, `rolling deployment`or `rolling update`. That's
why it is important to have all these features like API, event and object versioning. All together. Otherwise, it would
not be possible to achieve this. Sure, we still can use simplified approach with replays or stops etc. to avoid dealing
with api and data versions at all. We can deprecate old code and remove it later after deploy which is also valid
scenario. But that`s not the best way to deal with a problem. Avoidance is not a solution and solution will give us
benefits.

### Solution

The goal of Generon is not to ignore but to deal with these challenges, providing freedom of choice and the ability to
apply best practices. Another peace to make `rolling update` possible and be more flexible is logic versioning. It might
not be required frequently, but when we need, it is better to have it. To migrate `List<User>`
to `Long2ObjectHashMap<User>` we need to do next steps:

1. Add new field - `@SnapshotField(value = N, version = 2) Long2ObjectHashMap<User> userMap`
2. Use `disabledFrom` with previous
   field - `@SnapshotField(value = 1, disabledFrom = 2) List<User> userList`. `disabledFrom` is optional and used only
   for optimization purpose. It indicates that starting from `runtime version 2` this field will not be used and can be
   ignored completely for snapshots. It will speed up taking and applying snapshots by removing redundant work and use
   less memory at the runtime because this field will not contain any data, and should not be used starting from defined
   runtime version.
3. `@Inject MigrationComponent` and use `if (migrationComponent.currentRuntimeVersion() < K)` to understand whether
   logic should use old or new data structure. It allows to support multiple code versions at once.
4. Increment property value `generon.migration.current_code_version`. Default is `1`. It is needed for a safety to
   forbid versions from being greater than expected.
5. Add method `void migrate2()` with annotation `@MigrateTo(M)`, when `M` is a runtime version. When version is switched
   to `M` this method will be triggered. Inside this method we need to implement migration logic. In our case we need to
   take data from old structure and move it to the new one. The method is executed at runtime when a command to apply
   version `M` is received, in our case `M is 2`. At the beginning version is `1`. Alternatively we can
   use `@MigrateTo void migrate(int version)` instead of `@MigrateTo(M) void migrate()` to allow dynamically work with
   all versions.
6. Implement migration - `userList.forEach(user -> userMap.put(user.id, user));`
7. Use internal component `AdminComponent` and call `migrate()` method which will increment runtime version from 1 to 2
   and trigger our `@MigrateTo` methods.

To migrate event versions we just need to use `if (migrationComponent.currentRuntimeVersion() < M)` and trigger needed
event based on the runtime version.