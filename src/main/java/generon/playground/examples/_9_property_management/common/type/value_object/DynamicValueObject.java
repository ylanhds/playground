package generon.playground.examples._9_property_management.common.type.value_object;

import generon.core.annotation.Value;
import generon.core.annotation.scan.value.ValueObject;

import java.time.Duration;
import java.util.List;
import java.util.Map;

@ValueObject("dynamic-object")
public record DynamicValueObject(
        @Value("primitive.int") int primitive,
        @Value("boolean-array") boolean[] booleanArray,
        @Value(value = "non.existent", defaultValue = "123") long defaultValue,
        List<String> stringList,
        Map<String, Integer> map,
        Duration duration
) {
}
