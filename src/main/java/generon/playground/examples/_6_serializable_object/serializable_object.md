Classes annotated with `@Serializable` will be considered POJO, and it will be automatically integrated with protocol at
all levels. POJO fields can have all types supported by protocol - primitives, arrays, collections with nested generic
types, custom types, other serializable objects (nested).

All fields should be public. Circular dependencies are forbidden (User -> Address -> User)

### Object versions

For low level systems it is a big challenge to implement flexible version system because of different layouts of data.
Each version of same object can have different amount of fields and their types, meaning serialization/deserialization
process will not be aligned with old structure and data will not be correctly read by the new structure. It should have
backward compatibility, allow code base to evolve without difficulties, be easy to use and maintain. Generon solves this
issue by using annotations @FromVersion & @RemovedFromVersion on object fields. POJO becomes a version schema for
itself. It allows to implement complex version dependencies including nested object versions.

For example:

```java
public class User {

    int id;
    String name;

    @FromVersion(2)
    String fullAddress;

}
```

User now have two versions:
<br>
V1 - id, name
<br>
V2 - id, name, fullAddress

Generon will generate helper classes - `UserV1`, `UserV2` with own set of fields. These classes will be used in SDK on
client side to make sure that redundant fields are not set.

Now we can use `@ObjectVersion` annotation to specify which version to use:

```java
@ServiceMethod(1)
@ObjectVersion(2)
public void createUser(User user)
```

In this example on the method level we have only one root element - User. But in case there are multiple serializable
objects we need to use annotation on parameter level because version 2 will be applied to `all` serializable objects
including return type:

```java
@ServiceMethod(1)
public void createUser(@ObjectVersion(2) User user)
```

Generon will know at compilation time that this specific User parameter uses version 2, meaning protocol will write/read
all 3 fields. If we set version to 1 - protocol will ignore `fullAddress` field because it is added from the
version 2.

Let's assume that after some time we decided to create additional object `Address` and we want to move `fullAddress`
field there. This is how we can do this:

```java
public class User {

    int id;
    String name;

    @FromVersion(2)
    @RemovedFromVersion(3)
    String fullAddress;

    @FromVersion(3)
    Address address;

}
```

```java
public class Address {
    public String fullAddress;
}
```

Field `fullAddress` will disappear from User V3 and new field will be added - `Address address`:

UserV1 - id, name
<br>
UserV2 - id, name, fullAddress (String)
<br>
UserV3 - id, name, address (Address)

Address has 1 version.

Let`s assume that after some time we decided to split Address.fullAddress field into several more specific fields:

```java
public class Address {

    @FromVersion(1) // Can be removed because 1 is default
    @RemovedFromVersion(2)
    public String fullAddress;

    @FromVersion(2)
    public String country;

    @FromVersion(2)
    public String stateOrProvince;

    @FromVersion(2)
    public String city;

    @FromVersion(2)
    public String street;

    @FromVersion(2)
    public String postalCode;

    @FromVersion(2)
    public String buildingNumber;

}
```

To enable this new version of Address for the User object we need to do next:

```java
public class User {

    public int id;
    public String name;

    @FromVersion(2)
    @RemovedFromVersion(3)
    public String fullAddress;

    @FieldVersions(from = {
            @FromVersion(3), // Or fully - @FromVersion(value = 3, versions = @ObjectVersion(1))
            @FromVersion(value = 4, versions = @ObjectVersion(2)),
    })
    public Address address;

}
```

Now we can use `@ObjectVersion(4)` for the User type.

Note! If we already have method with `@ObjectVersion(N)`, we can not just change it to `@ObjectVersion(M)` because
protocol will not recognize fields of old data, it will use only one version which is statically defined. To fix that we
need to create new method with next version:

```java
@ServiceMethod(value = 1, version = 2)
public void createUserV4(@ObjectVersion(4) User user)
```

Now `SDK` has two methods with different versions of User.

But that's not fully flexible. Let's imagine that method has lots of serializable objects. It means that for each
version update of each object we need to create method with new version, copy logic and maintain it. What if we just
don`t want to have another method version ? Generon has a solution for this too.

### Dynamic object version

To allow protocol to recognize multiple versions at once we need to set a version dynamically (at runtime). To do that
we need to add metadata field:

```java
public class User {

    @Version
    byte version

    int id;
    String name;

    @FromVersion(2)
    @RemovedFromVersion(3)
    String fullAddress;

    @FieldVersions(from = {
            @FromVersion(3), // Or fully - @FromVersion(value = 3, versions = @ObjectVersion(1))
            @FromVersion(value = 4, versions = @ObjectVersion(2)),
    })
    Address address;

}
```

Now we can remove `@ObjectVersion` annotation from the method completely and use `metadata field` to determine
object `version` at runtime:

1. Interface:
   ```java

   @ServiceMethod(1)
   public void createUser(User user)

   ```
2. Implementation should just use `user.version` to determine version, use only fields from this version.

Now, from all places when we need to send (write) object we need to set version manually using `user.version = N` and
set only fields from this version.

Also, in case `List<User>`, `User[]` and other collection types we need to specify version for each user object. It
gives such possibilities like storing different versions in same data structure with @SnapshotField or sending
collection with different versions to the server or/and receiving it back.

Note!

1. If there is no `@ObjectVersion` annotation and `object does not have @Version field defined` Generon will
   use `version 1` by default even if object has schema for multiple versions.
2. If there is no `@ObjectVersion` annotation and `object has @Version field defined` - dynamic approach should be used,
   when sender side should specify object version manually - `user.version = N;` and receiver side should or can use
   this field to determine object version. If version will not be assigned, exception will be thrown during write/send
   process because it will be 0 by default. If we want to skip defining version each time we can
   do `public byte version = N;` inside User class to ensure that version is always set to some initial value.
3. If there is `@ObjectVersion` annotation, version from the annotation will be used in any case, even if `@Version`
   field is specified. If `@ObjectVersion(1)` is defined but on sender side `user.version = N` is used it will be
   ignored and set to 1 anyway.
4. If there is single version of the object, version 1 will be used in any case.

If we had `@ObjectVersion(N)` and we decided to use dynamic versions we can just add `@Version` field and remove
`@ObjectVersion(N)` annotation completely. Old events, commands and data structures already have header for it with it`s
version.

For nested objects it can work in the same way. If `Address` will define `@Version`, but we leave all as is, current
User's configuration from our example will become invalid because `@FromVersion(3)` for address field does not define
Address version itself, meaning dynamic version for address will be used from now. To make `UserV3` work correct as it
was before we need to update `@FromVersion(3)` and specify address version as 1 because before `Address` version was not
dynamic and 1 was a default value but not after we add `@Version` field. We can always specify versions even if it
is `V1` to avoid issues in case we are moving to dynamic version:

```java
@FieldVersions(from = {
        @FromVersion(value = 3, versions = @ObjectVersion(1))
        @FromVersion(value = 4, versions = @ObjectVersion(2)),
})
Address address;
```

If we want to have dynamic version for `User.address` too, we could just remove all Address version configs:

```java
@FromVersion(3)
Address address;
```

It will cover also previous `FromVersion(4)` because now version is dynamic anyway. In such case we will need to write
ifs for user version and also starting from version 3 we need to write ifs for address too, meaning it will have nested
ifs per each version - in case we need to handle all cases.

Note! We can not put it back for the same version. We can remove `@ObjectVersion` and start to use dynamic version, but
after we used `dynamic version` we can not put back `@ObjectVersion(N)` because there may already be objects with
different versions, and it will break old commands, events and data structures because it will treat all objects as
version N. So, if you started to use `dynamic version` you can not turn it back for same version, but you can change it
from the next one again:

```java
@FieldVersions(from = {
        @FromVersion(value = 3, versions = @ObjectVersion(1))
        @FromVersion(value = 4, versions = @ObjectVersion(2)),
        @FromVersion(5),
})
Address address;
```

It means that starting from `User V5` we will need to use `Address.version` dynamically. From UserV6 we can turn it
back:

```java
@FieldVersions(from = {
        @FromVersion(value = 3, versions = @ObjectVersion(1))
        @FromVersion(value = 4, versions = @ObjectVersion(2)),
        @FromVersion(5),
        @FromVersion(value = 6, versions = @ObjectVersion(2)),
})
Address address;
```

### Validation

It is very useful to validate version before writing and reading. Real world use case is when cluster is updated for
newer version but client not yet updated, or opposite, when client is updated but cluster not. Command or event with
new object version can be sent, but another side still think that object version is static and is for example 2. It will
corrupt read and in worse scenario it may read incorrect offsets without any exception, leading to dangerous behavior
and wrong calculations. Or, server can have new event method version or another method which client is not aware of,
meaning it will never process it correctly. Just skipping a message is not what we usually want. The best what we can do
is to stop processing completely, throw an exception and when client is updated we will continue processing from when we
finished. It is much better to have this approach comparing to processing wrong data or skipping message which will lead
to incorrect state and can not be easily recovered. It is always good practice to validate data before processing.

To avoid such issues we can do next:

1. Update receiver side first. It means that if there are updates to APIs, update the server before anything else.
   Similarly, if events are updated, ensure the clients are updated first.
2. Update all components of the application in any order, but ensure that the new version of the code can not be
   executed until every part of the application is fully updated.

Exception will be thrown in next cases:

1. If receiver has `@ObjectVersion(N)` configuration but gets object with `version M` (Runtime).
2. If receiver has `dynamic version` configuration but gets object with version which is unknown. For example object
   has `maximum version of 2`, meaning only `1` and `2` versions are allowed, but received `3`. (Runtime)
3. If writer has `dynamic version` configuration and sends unknown version. For example when we
   do `user.version = UNKNOWN;` manually it will be invalid. If we have `@ObjectVersion(N)` annotation or
   object has only 1 version, it will be valid case because `user.version` ignored completely. (Runtime).
4. If object has 2 versions, `@ObjectVersion(N)` annotation can have only `1` or `2` value. (Compilation time).

`@ObjectVersion(N)` configuration will allow only version `N`. `Dynamic version` configuration will allow only known
range of versions. It means that sender can still have `@ObjectVersion(N)` but receiver can already
have `dynamic version` configuration which allows to handle `N` and other versions - it is valid case. However, it is
preferable to keep all application versions synchronized to avoid unexpected and undesirable behavior.

These validations will work at all levels including API calls, events, snapshots, service/producer class versions,
service/event method versions, nested object versions.

### Summary

Generon provides `powerful`, `flexible` and `safe` solution for complex challenge such as data versioning. It allows you
to define `version schema` for POJOs including nested objects, adding new and removing old fields. It
supports `dynamic version` setup which allows to decide a version at runtime. Also, it has version validations which
will ensure that data format is always correct.

Maintaining all possible versions of an application and ensuring full backward compatibility is a good practice. It
allows to replay all commands and events from the beginning, avoiding potential issues. Sometimes in production
environment Raft Log is manually truncated (to some snapshot position) because it becomes too big, meaning old version
of data will not be read, and we can remove old schema versions with code which uses it. Another reason to do so is
because it becomes hard to maintain multiple versions at the same time. It is not good practice because who knows what
can happen. There may be critical bug and after a fix we need to replay big set of events to recover correct and fixed
state. If log becomes too big it could be just moved to the separate archive storage, but not be completely removed.
Versioning feature helps to implement `rolling updates` - deploy new code without stopping the cluster.