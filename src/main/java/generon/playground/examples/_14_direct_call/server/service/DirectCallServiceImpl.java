package generon.playground.examples._14_direct_call.server.service;

import generon.core.annotation.SnapshotField;
import generon.playground.examples._14_direct_call.common.service.DirectCallService;
import org.agrona.collections.IntArrayList;

public class DirectCallServiceImpl implements DirectCallService {

    @SnapshotField(1)
    private final IntArrayList intArrayList = new IntArrayList();

    @Override
    public void addNumber(int number) {
        intArrayList.add(number);
    }

    @Override
    public IntArrayList getNumbers() {
        return intArrayList;
    }
}
