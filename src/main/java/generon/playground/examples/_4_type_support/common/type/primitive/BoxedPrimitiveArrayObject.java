package generon.playground.examples._4_type_support.common.type.primitive;

import generon.core.annotation.scan.serializable.Serializable;

import java.util.Arrays;
import java.util.StringJoiner;

@Serializable
public class BoxedPrimitiveArrayObject {

    public Boolean[] booleans;
    public Byte[] bytes;
    public Short[] shorts;
    public Character[] chars;
    public Float[] floats;
    public Integer[] ints;
    public Double[] doubles;
    public Long[] longs;

    public BoxedPrimitiveArrayObject() {
    }

    public BoxedPrimitiveArrayObject(Boolean[] booleans, Byte[] bytes, Short[] shorts, Character[] chars, Float[] floats, Integer[] ints, Double[] doubles, Long[] longs) {
        this.booleans = booleans;
        this.bytes = bytes;
        this.shorts = shorts;
        this.chars = chars;
        this.floats = floats;
        this.ints = ints;
        this.doubles = doubles;
        this.longs = longs;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BoxedPrimitiveArrayObject.class.getSimpleName() + "[", "]")
                .add("booleans=" + Arrays.toString(booleans))
                .add("bytes=" + Arrays.toString(bytes))
                .add("shorts=" + Arrays.toString(shorts))
                .add("chars=" + Arrays.toString(chars))
                .add("floats=" + Arrays.toString(floats))
                .add("ints=" + Arrays.toString(ints))
                .add("doubles=" + Arrays.toString(doubles))
                .add("longs=" + Arrays.toString(longs))
                .toString();
    }

}
