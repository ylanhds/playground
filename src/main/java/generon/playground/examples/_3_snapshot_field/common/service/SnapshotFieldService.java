package generon.playground.examples._3_snapshot_field.common.service;

import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;
import org.agrona.collections.IntArrayList;

@Service(Constants.Id.Service.SNAPSHOT_FIELD)
public interface SnapshotFieldService {

    @ServiceMethod(1)
    IntArrayList addNumbers(int[] newNumbers);

}
