### What is root element configuration ?

Root element configuration - is a way to configure types to behave differently at Generon protocol level, optimize some
types for better performance or define additional behavior.

#### Root element is an element defined at root level of a type:

1. `Map<String, User>` has 3 root elements - `Map`, `String`, `User`.
2. `MyEnum` - has 1 root element - `MyEnum`
3. `Map<Long, List<String>>` - has 4 root elements - `Map`, `Long`, `List`, `String`

#### List of all annotations used for root element configuration:

@StringType<br>
@EnumType, @EnumTypes<br>
@ObjectVersion, @ObjectVersions<br>
@ReturnValueConfig<br>
@RootElementConfig

#### Currently, we can configure next types:

1. `String`. It can have UTF-8 or ASCII encoding. Use `@StringType(StringSerialization.UTF_8/ASCII)`. Default is UTF-8
2. `Enum`. It can be identified by string name or ordinal id.
   Use `@EnumType(type = MyEnum.class, value = EnumSerialization.STRING/ORDINAL)`. Default is STRING
3. `Serializable object`. It can have different versions with different amount of fields and their configurations.
   Use `@ObjectVersion(type = User.class, value = 2)`. Default is 1 in case @Version field is not defined

If we need to configure multiple root elements at same level we can use array
equivalents - `@EnumTypes`, `@ObjectVersions` or Use `@RootElementConfig` which has all in one. If multiple annotations
are used they will be merged together.

If a type has single root element we can skip defining `type` attribute for `@EnumType` and `@ObjectVersion`. If a type
has multiple root elements we can specify `type` config for each element separately, or we can remove `type` definition
to apply same for all types.

For example:

1. @EnumType(EnumSerialization.ORDINAL) MyEnum simpleEnum;
2. @EnumType(EnumSerialization.ORDINAL) List<MyEnum> myEnums;
3. @EnumType(EnumSerialization.ORDINAL) Map<MyEnum1, MyEnum2> myEnums; // Applied to both enums
4. @EnumTypes({<br>&nbsp;&nbsp;&nbsp;&nbsp;@EnumType(type = MyEnum1.class, value = EnumSerialization.ORDINAL),<br>
   &nbsp;&nbsp;&nbsp;&nbsp;@EnumType(type = MyEnum2.class, value = EnumSerialization.STRING)<br>})<br>Map<MyEnum1,
   MyEnum2> myEnums; // Applied to both enums separately
5. @StringType(StringSerialization.ASCII)
   <br>
   @EnumType(EnumSerialization.ORDINAL)
   <br>
   @ObjectVersion(2)
   <br>
   Map<String, Map<MyEnum, User>> complexMap;

#### On the method level we can use same annotations including @ReturnValueConfig which is used specifically for return type configuration.

Example of complex method configuration:

```java
@ServiceMethod(1)
@EnumType(EnumSerialization.ORDINAL)
@ReturnValueConfig(
        stringType = @StringType(StringSerialization.ASCII),
        versions = @ObjectVersion(2)
)
public Map<String, User> findUsers(MyEnum1 myEnum1,@EnumType(EnumSerialization.STRING) @MyEnum2 myEnum2,@ObjectVersion(3) MyObject myObject)
```

Config can be defined on the method level which will work for all method root elements - return type and parameter
types, all together. We can override config by defining it more specific - use @ReturnValueConfig for return type, or
annotations at parameter level.

`@EnumType` and `@StringType` annotations used for optimization, while `@ObjectVersion` is used to define object version
configuration.

These configurations are also working with `@SnapshotField` and `serializable object fields` and
are `NOT backward and forward compatible`.