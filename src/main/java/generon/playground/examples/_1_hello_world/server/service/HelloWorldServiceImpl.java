package generon.playground.examples._1_hello_world.server.service;

import generon.playground.examples._1_hello_world.common.service.HelloWorldService;

public class HelloWorldServiceImpl implements HelloWorldService {

    @Override
    public String hello() {
        return "Hello, Generon";
    }

    @Override
    public void printHello() {
        System.out.println("Server: " + hello());
    }

}
