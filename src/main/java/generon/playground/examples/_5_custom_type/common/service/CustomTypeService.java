package generon.playground.examples._5_custom_type.common.service;

import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;
import generon.playground.examples._5_custom_type.common.type.buffer_object.InstantBO;

import java.time.Instant;
import java.util.List;

@Service(Constants.Id.Service.CUSTOM_TYPE)
public interface CustomTypeService {

    @ServiceMethod(1)
    Instant instantTC(Instant instant);

    @ServiceMethod(2)
    InstantBO instantBO_1(InstantBO instantBO);

    @ServiceMethod(3)
    InstantBO instantBO_2(Long timestamp);

    @ServiceMethod(4)
    Long instantBO_3(InstantBO instantBO);

    @ServiceMethod(5)
    Instant instantBO_4(InstantBO instantBO);

    @ServiceMethod(6)
    List<Instant> genericTypes(List<InstantBO> instantBOList);
}
