package generon.playground.examples._5_custom_type.server.service;

import generon.playground.examples._5_custom_type.common.service.CustomTypeService;
import generon.playground.examples._5_custom_type.common.type.buffer_object.InstantBO;

import java.time.Instant;
import java.util.List;

public class CustomTypeServiceImpl implements CustomTypeService {

    private final InstantBO instantBO = new InstantBO(); // Flyweight

    @Override
    public Instant instantTC(Instant instant) {
        return instant;
    }

    @Override
    public InstantBO instantBO_1(InstantBO instantBO) {
        return instantBO;
    }

    @Override
    public InstantBO instantBO_2(Long timestamp) {
        InstantBO.Writer writer = instantBO.writer();

        if (timestamp == null) {
            writer.wrapAsNull();
            return instantBO;
        }

        writer.wrap(timestamp);
        return instantBO;
    }

    @Override
    public Long instantBO_3(InstantBO instantBO) {
        InstantBO.Reader reader = instantBO.reader();

        if (reader.isNull) {
            return null;
        }

        return reader.timestamp;
    }

    @Override
    public Instant instantBO_4(InstantBO instantBO) {
        InstantBO.Reader reader = instantBO.reader();
        return reader.asInstant();
    }

    @Override
    public List<Instant> genericTypes(List<InstantBO> instantBOList) {
        return instantBOList.stream()
                .map(instantBO1 -> instantBO1.reader().asInstant())
                .toList();
    }

}
