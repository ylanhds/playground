package generon.playground.examples._2_exception_handler.server.service;

import generon.integration.objects.GeneronProcessingException;
import generon.playground.configuration.Utils;
import generon.playground.examples._2_exception_handler.common.service.ExceptionHandlerService;

public class ExceptionHandlerServiceImpl implements ExceptionHandlerService {

    @Override
    public String testException(int actionId) {
        if (actionId == 1) {
            throw new GeneronProcessingException(42, "Expected exception. Action 1");
        }

        if (actionId == 2) {
            throw new IllegalStateException("Expected exception. Action 2");
        }

        if (actionId == 3) {
            Utils.sleep(200); // Trigger timeout on the client side
            return "Timeout response. Should not be received";
        }

        return "Success response";
    }

}
