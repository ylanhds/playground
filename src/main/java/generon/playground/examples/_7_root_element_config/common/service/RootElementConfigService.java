package generon.playground.examples._7_root_element_config.common.service;

import generon.core.annotation.scan.serializable.migration.ReturnValueConfig;
import generon.core.annotation.scan.serializable.migration.RootElementConfig;
import generon.core.annotation.scan.serializable.migration.object.EnumType;
import generon.core.annotation.scan.serializable.migration.object.ObjectVersion;
import generon.core.annotation.scan.serializable.migration.object.StringType;
import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.core.objects.EnumSerialization;
import generon.core.objects.StringSerialization;
import generon.playground.configuration.Constants;
import generon.playground.examples._7_root_element_config.common.type.Book;
import generon.playground.examples._7_root_element_config.common.type.ObjectWithRootConfig;
import generon.playground.examples._7_root_element_config.common.type.SimpleEnum;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service(Constants.Id.Service.ROOT_ELEMENT)
public interface RootElementConfigService {

    @ServiceMethod(1)
    @RootElementConfig(stringType = @StringType(StringSerialization.ASCII))
    String string(String asciiString);

    @ServiceMethod(2)
    @RootElementConfig(enumTypes = @EnumType(EnumSerialization.ORDINAL))
    SimpleEnum enumType(SimpleEnum ordinalEnum);

    @ServiceMethod(3)
    @RootElementConfig(versions = @ObjectVersion(1))
    ObjectWithRootConfig objectWithRootConfigV1(ObjectWithRootConfig object);

    @ServiceMethod(4)
    @RootElementConfig(versions = @ObjectVersion(2))
    ObjectWithRootConfig objectWithRootConfigV2(ObjectWithRootConfig object);

    @ServiceMethod(5)
    @ReturnValueConfig(
            stringType = @StringType(StringSerialization.ASCII),
            versions = @ObjectVersion(2)
    )
    Map<String, List<Book>> findBooksByGenre(@StringType(StringSerialization.ASCII) Set<String> genres);

}
