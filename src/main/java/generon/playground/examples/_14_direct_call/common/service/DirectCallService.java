package generon.playground.examples._14_direct_call.common.service;

import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;
import org.agrona.collections.IntArrayList;

@Service(Constants.Id.Service.DIRECT_CALL)
public interface DirectCallService {

    @ServiceMethod(1)
    void addNumber(int number);

    @ServiceMethod(value = 2, directRequest = true)
    IntArrayList getNumbers();

}
