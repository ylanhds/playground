package generon.playground.examples._7_root_element_config.common.type;

import generon.core.annotation.scan.serializable.Serializable;
import generon.core.annotation.scan.serializable.migration.field.FieldVersions;
import generon.core.annotation.scan.serializable.migration.field.FromVersion;
import generon.core.annotation.scan.serializable.migration.object.EnumType;
import generon.core.annotation.scan.serializable.migration.object.StringType;
import generon.core.objects.EnumSerialization;
import generon.core.objects.StringSerialization;

@Serializable
public class ObjectWithRootConfig {

    @FromVersion(value = 1, stringType = @StringType(StringSerialization.ASCII))
    public String value;

    @FieldVersions(from = {
            @FromVersion(1), // EnumSerialization.STRING is default
            @FromVersion(value = 2, enumTypes = @EnumType(EnumSerialization.ORDINAL)),
    })
    public SimpleEnum simpleEnum;

}
