### General overview

`Multitenancy` is a design where multiple independent clients (tenants) can be hosted inside a `single cluster` or
database, which is called a `shared environment`. From logic perspective it means to have data separated among all
tenants and not be mixed. As example, we could have key-value database, and we need to allow separate clients / tenants
to be able to use same key. Sure, we could have additional map to separate tenant data by id, but it will not work well
long term when amount of logic and data will grow. Generon supports it out of the box, which brings flexibility and
removes additional complexity for developing and maintaining multi-tenant solution.

### Disadvantages of implementing multi-tenancy `as is`

1. In most cases we `do not` need to interact with `multiple tenants within single request`, which will `force` us to
   use some abstraction layers to separate tenants which `needed to be maintained`.
2. Over time when project will have `more data and dependencies` between components it will be `more challenging` to
   keep `code quality` and `performance` on a good level, due to mixed datastructures.
3. Adding `new tenant` or `removing existing` one requires careful data state management
   and `additional layer to maintain` among all components, which `brings room for bugs and code quality degradation`.
4. `Single command` can require `multiple map lookups` to gather needed information during the processing, which can
   cause `performance degradation`. To avoid this we could use a map like `tenantById` with big `TenantData` object as a
   value. Such approach is ok, but `it removes flexibility` and will `challenge single responsibility principle` over
   time.
5. Common use case for such kind of systems is to `access information or statistics` from `multiple tenants`, for
   example to develop dashboard to see all tenants and their information, which requires to have two logically separated
   scopes - `tenant` and `global`, which is above tenants. Mixing them will cause code quality degradation.
6. Snapshot, event, configuration and property management, dependency injection, client SDK and many other parts of the
   application will require `lots of additional work` to keep getting `same value`, `code quality` and have
   multi-tenancy at the same time.

### Generon's solution

1. #### Scopes.
   `@Component`, `@Configuration`, `@Service` annotations have attribute `scope` which can be `TENANT` (default)
   or `GLOBAL`. Annotations like `@PropertyLoader`, `@InjectionElement`, `@SnapshotField`, `@Init`, `@MigrateTo` and
   others, will work under configured scope independently, per tenant or global scope.
2. #### Dependency injection.
   Tenant components can use `@Inject` and `@Value` annotations to inject components or properties from the same tenant
   scope and from global scope. Tenant injections will have priority over global ones in case conflicts by property key
   for `@Value` or a type for `@Inject` and `qualifier`. Component type conflicts can only be for cases
   with `@InjectionElement`.
3. #### `TenantScope<T>` injection.
   Global scope can interact with tenants and their components using `@Inject` or `@Value` and `TenantScope<T>` field
   declaration. Let's assume we need to make an API which will collect amount of users from all tenants and return it as
   a map with tenantId as a key and user amount as a value. We have `UserService` with `Tenant` scope, which stores user
   information. We can make `Global` scope `@Service` which will collect info from all tenants. This global service can
   inject `UserService` instances `from all tenants` by
   using `@Inject private TenantScope<UserService> tenantUserServices`. TenantScope is a simple object which holds value
   per tenantId with `generic type` matched from `tenant scope type` and we can use `forEach` to iterate through all and
   collect user information for each tenant or use `collect` method which can accumulate result, similar to "reduce". If
   we want to access UserService for tenant 2 we can do `UserService userSercice = tenantUserServices.type(2)`.
   When `tenant is added` or `removed` at runtime, all `TenantScope` fields from all components will be updated
   automatically to represent current state of tenants.
   <br>
   <br>
   We can inject property from all tenants using `@Value("my_list") private TenantScope<List<String>>`, which in this
   case can be comma separated value parsed to `List<String>`. Same types supported as usual injections without
   TenantScope. `@Inject(qualifier = "...")` and `@Value(defaultValue = "...")` are supported too.
   <br>
   <br>
   Tenant information inside injected `TenantScope` fields will not be present during `@Init` annotation step due to
   order of internal initialization process. It is tradeoff about initialization order between global and tenant scope
   components. `Global scope is initialized first` and `tenant scope right after`, it means that global scope can not
   see any tenant information during initialization, but global scope components can already be seen inside tenant scope
   during `@Init` executions. To be able to use `TenantScope<T>` for global scope during initialization and have tenant
   scope initialized, we can use additional `@PostInit` annotation which has exactly same behavior as `@Init` but is
   triggered after all scopes initialized and `@Init` is executed. It has same execution order - `global scope first`
   and `tenant scope second`. Tenant scope components `can not` inject `TenantScope` and produce `TenantScope` elements
   via `@InjectionElement`. Which means `one tenant can not access another one`. It is what we actually need for
   multi-tenancy - `encapsulation` and data separation. But if you really need it, and you know what you are doing,
   you can inject global scope component which has `TenantScope` field and use it an access bridge to other tenants.
4. #### Events.
   `@EventProducer` is always global. All events from all scopes and tenants will go to same streams and can be
   separated on consumer side by using eventHeader.tenantId. `Global` scope has `tenantId 0`. Event `tenantId` is based
   on scope under which it is written, based on `request header` from the `CommandContext`. It means that if there
   is `Global` scope component which actually writes an event and `Tenant` scope `@Service` injects it and uses it, the
   event `will not be` written with `tenantId 0`, because request is made from the tenant scope, and it will use
   tenantId from the request anyway. If `@Service` is `GLOBAL` and uses `Global` or `Tenant` component with event
   producer, tenantId will be 0 because it was requested from global scope API. Mixing events between scopes is not a
   good practice, but can be used in some cases. Consumer will always know real tenantId and scope of the event under
   which it was triggered.
5. #### Properties.
   `Static properties` from `generon.properties` or similar files under `resources`
   directory `considered as global scope` properties. `Global` scope can have own `@PropertyLoader` as usual. `Tenant`
   specific properties can be integrated via `@PropertyLoader` and they have priority over global scope properties in
   case conflicts. All expressions and injection types supported as well.
6. #### Client SDK.
   `ClientAPI` class has getters for `GlobalClientAPI` and `TenantClientAPI`, we can get these object
   using `GlobalClientAPI globalAPI = clientAPI.globalAPI()` or `TenantClientAPI tenantAPI = clientAPI.tenantAPI(1)` by
   providing tenantId. We can create TenantClientAPI for multiple tenants. Services with `Global` scope will appear
   under `GlobalClientAPI`, `Tenant` ones will appear under `TenantClientAPI`. In case API call is made
   for `unknown tenantId` exception will be thrown.
7. #### Timer events.
   Timers are always global internally, but it is logically separated. If timer was scheduled from tenant 3,
   only `@TimerEventHandler` methods from tenant 3 will be executed.
8. #### Migrations.
   `@MigrateTo` methods will be triggered for all scopes. For global first and for tenants after.
9. #### Snapshots.
   `@SnapshotField` will be serialized and deserialized keeping tenant data isolation. Global scope processed first and
   tenant scope after.
10. #### Tenant management.
    To `add new` tenant or `remove existing` one we can use internal `Global` scope component `AdminComponent` with
    methods `int addTenant()` and `void removeTenant(int tenantId)`. When cluster started from scratch with empty
    state `first tenant is created with id 1 by default`. We can do `adminComponent.removeTenant(1)` and have 0 tenants,
    and add new ones after. When `last tenant is removed` id counter will be `reset`, so `first tenant` will have `id 1`
    when it is added. Global state will always remain separate and will not be touched. If project does not need
    multi-tenancy we can use hardcoded `tenantId 1` on the client side, but it still a good practice to separate
    components by scope, which will ensure good code quality long term. Generon does not have two build modes - single
    tenant build (without scope separation) and multi-tenant build, that's why `multitenancy is always enabled` even
    when there is only 1 tenant. It is trade off decision. The reason is that adding new tenant or removing existing one
    should be a part of Raft Log, same as `takeSnapshot` command. It can not be resolved at build time and ensure
    correct replays. When project needs to switch from single tenant to multi-tenant this possibility will be available
    with almost no code changes and additional overhead. Two modes would have a bit different generated classes, method
    signatures etc. which will make learning and migration more complex. It is easy to remove overhead by creating
    simple aggregation class which will combine two scopes with hardcoded tenantId 1 for tenant API and have delegation
    methods for factories.
    <br>
    <br>
    When new tenant is added, at first it will call all `@InjectionElement` and `@PropertyLoader` methods to initialize
    tenant state and process DI. Second, `@Init` and `@PostInit` methods will be executed. Third, it will
    run `@MigrateTo` methods for all past migrations till current migration version. `Even if state is empty` and there
    is nothing to migrate, migration methods `still can have` some logic which will `initialize` the state and not just
    remap data structures. For that reason, it requires to execute migration methods also to have correct and consistent
    state.