package generon.playground.configuration;

import generon.integration.cluster.client.GeneronClientContext;
import generon.integration.cluster.server.GeneronNodeContext;
import generon.integration.objects.ErrorLogStrategy;
import io.aeron.driver.exceptions.InvalidChannelException;
import org.agrona.ErrorHandler;

import java.util.function.Consumer;

public class TestConfigurator {

    private Consumer<GeneronNodeContext> nodeContextConfigurer;
    private Consumer<GeneronClientContext> clientContextConfigurer;

    public TestConfigurator() {
    }

    public TestConfigurator(Consumer<GeneronNodeContext> nodeContextConfigurer, Consumer<GeneronClientContext> clientContextConfigurer) {
        this.nodeContextConfigurer = nodeContextConfigurer;
        this.clientContextConfigurer = clientContextConfigurer;
    }

    /**
     * Disable error handler. It is used to remove expected exception stack trace from the console.
     * Do not do this in real life:)
     */
    public TestConfigurator disableErrorLogs() {
        customizeNodeContext(nc -> nc.errorContext(null, ErrorLogStrategy.NEVER));
        customizeClientContext(cc -> cc.errorContext(null, ErrorLogStrategy.NEVER));

        return this;
    }

    /**
     * InvalidChannelException happens on Aeron side when direct publications are created and closed very fast like during the replay.
     * It will be thrown when publication is created with existent combination of ip:port, sessionId and streamId.
     * The easiest way to reproduce is:
     * <pre>{@code
     * Publication publication;
     * for (int i = 0; i < 2; i++) {
     *      publication = aeron.addPublication("aeron:udp?endpoint=localhost:11001|session-id=0", 1);
     *      publication.close();
     * }
     * </pre>
     * 'close' action will not happen in sync with 'addPublication' and that`s the reason of such exception.
     * It is not a problem at all. It will recover automatically by retrying publication creation which will
     * succeed right after previous one is closed.
     * THe only issue is redundant error log. That`s it :)
     */
    public TestConfigurator disableInvalidChannelErrorLogs() {
        return disableNodeErrorLogs(InvalidChannelException.class);
    }

    public <E extends Throwable> TestConfigurator disableNodeErrorLogs(Class<E> throwableClass) {
        customizeNodeContext(nc -> {
            ErrorHandler errorHandler = nc.errorContext().errorHandler();

            if (errorHandler == null) {
                return; // All is disabled already
            }

            nc.errorHandler(throwable -> {
                if (throwable.getClass().equals(throwableClass)) {
                    return; // Skip needed exception
                }

                errorHandler.onError(throwable);
            });
        });

        return this;
    }

    /**
     * Change request timeout. Defaults are 5000L and 100 (ms)
     */
    public TestConfigurator requestTimeout(long requestTimeout, int checkInterval) {
        customizeClientContext(cc -> cc.timeoutContext().requestTimeout(requestTimeout).checkInterval(checkInterval));
        return this;
    }

    public void configure(GeneronNodeContext generonNodeContext) {
        if (nodeContextConfigurer == null) {
            return;
        }

        nodeContextConfigurer.accept(generonNodeContext);
    }

    public void configure(GeneronClientContext generonClientContext) {
        if (clientContextConfigurer == null) {
            return;
        }

        clientContextConfigurer.accept(generonClientContext);
    }

    private void customizeNodeContext(Consumer<GeneronNodeContext> ncConsumer) {
        if (nodeContextConfigurer == null) {
            this.nodeContextConfigurer = ncConsumer;
            return;
        }

        Consumer<GeneronNodeContext> previousConfigurer = this.nodeContextConfigurer;
        this.nodeContextConfigurer = nc -> {
            previousConfigurer.accept(nc);
            ncConsumer.accept(nc);
        };
    }

    private void customizeClientContext(Consumer<GeneronClientContext> ccConsumer) {
        if (clientContextConfigurer == null) {
            clientContextConfigurer = ccConsumer;
            return;
        }

        Consumer<GeneronClientContext> previousConfigurer = this.clientContextConfigurer;
        this.clientContextConfigurer = cc -> {
            previousConfigurer.accept(cc);
            ccConsumer.accept(cc);
        };
    }

}
