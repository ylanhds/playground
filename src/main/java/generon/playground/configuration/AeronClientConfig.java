package generon.playground.configuration;

import generon.integration.cluster.client.AeronClientProperties;
import generon.integration.cluster.server.GeneronClusterMember;
import generon.integration.cluster.server.GeneronNodeClientContext;

public class AeronClientConfig {

    public static final int EGRESS_PORT = 11000;
    public static final int DIRECT_SUBSCRIPTION_PORT = 11001;
    public static final int DIRECT_SUBSCRIPTION_STREAM_ID = 1;

    public static AeronClientProperties of(GeneronClusterMember generonClusterMember) {
        return AeronClientProperties.of(
                "localhost", generonClusterMember, EGRESS_PORT,
                DIRECT_SUBSCRIPTION_PORT, DIRECT_SUBSCRIPTION_STREAM_ID
        );
    }

    public static GeneronNodeClientContext toNodeClientContext() {
        return new GeneronNodeClientContext(DIRECT_SUBSCRIPTION_PORT, DIRECT_SUBSCRIPTION_STREAM_ID);
    }

}