package generon.playground.examples._15_multitenancy.common.service;

import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;
import org.agrona.collections.IntArrayList;

@Service(Constants.Id.Service.TENANT_SCOPE)
public interface TenantScopeService {

    @ServiceMethod(1)
    void addNumber(int number);

    @ServiceMethod(2)
    IntArrayList getNumbers();

}
