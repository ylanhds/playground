### Problem

@Serializable object can also be considered as a custom type, but it can contain only known types or other @Serializable
objects. What if we want to integrate a type which already exist in JDK or another library, and we can not or do not
want to modify this class ? That`s why we need to have a possibility to write something own and deed it to the Generon
Protocol.

### Solution

`TypeConverter` & `BufferObject` are the two ways of declaring custom type:

1. `TypeConverter`. We can create a class with `@TypeConverted` which implements `TypeConverterTemplate<T>` interface
   with read/write methods, where T is type we want to integrate. It ss relatively simple and registers type directly.
2. `BufferObject` interface is designed to wrap a buffer segment and have full control over it. BufferObject itself
   should never be null as parameter or value - meaning call `api.instantBO_1(null)` is invalid and will throw NPE.
   BufferObject has reader and writer interfaces. Two implementations should be in one object due to inability to split
   it for case with `@SnapshotField`, we want field to be defined as single direct type. If we are sending data - use
   writer, if we are receiving - reader.

Note. Type converter and buffer object have read/write methods which should return new offset. It should read and write
same amount of data and be consistent. Both approaches must be implemented very carefully. Each read/write should work
consistent and operate with the same amount of data for the same state. If it is not consistent it will corrupt entire
message, next data structures in a sequence, or entire snapshot when interface is used to serialize/deserialize type. It
will lead to unexpected behavior or/and even worse - no exceptions can be thrown at all and data will be wrongly
represented.