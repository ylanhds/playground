package generon.playground.examples._13_migration_management.client.event_listener;

import generon.client.migration_event_producer.MigrationEventListener;
import generon.integration.cluster.objects.header.EventHeader;

import java.util.concurrent.CountDownLatch;

public class MigrationEventListenerImpl implements MigrationEventListener {

    private final CountDownLatch countDownLatch;

    public MigrationEventListenerImpl(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void onEventV1(int id, EventHeader header) {
        System.out.println("Consumed eventV1. id: " + id);
        countDownLatch.countDown();
    }

    @Override
    public void onEventV2(int id, boolean flag, EventHeader header) {
        System.out.println("Consumed eventV2. id: " + id + ", flag: " + flag);
        countDownLatch.countDown();
    }

}
