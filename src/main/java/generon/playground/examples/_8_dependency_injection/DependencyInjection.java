package generon.playground.examples._8_dependency_injection;

import generon.client.ClientAPI;
import generon.client.TenantClientAPI;
import generon.client.dependency_injection_service.DependencyInjectionServiceBlockingAPI;
import generon.client.dependency_injection_service.DependencyInjectionServiceFactory;
import generon.playground.configuration.Example;

public class DependencyInjection implements Example {

    @Override
    public void run(ClientAPI clientAPI) {
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        DependencyInjectionServiceFactory dependencyInjectionServiceFactory = tenantAPI.dependencyInjectionServiceFactory();
        DependencyInjectionServiceBlockingAPI api = dependencyInjectionServiceFactory.blocking();

        System.out.println("weekends: " + api.weekends());
        System.out.println("workdays: " + api.workdays());
        System.out.println("allWeekDays: " + api.allWeekDays());
        System.out.println("primitiveInt: " + api.primitiveInt());
        System.out.println("boxedInt: " + api.boxedInt());
        System.out.println("string: " + api.string());
        System.out.println("duration: " + api.duration());
        System.out.println("globalScopeString: " + api.globalScopeString());
        System.out.println("nextEntityId: " + api.nextEntityId());
        System.out.println("startupTime: " + api.startupTime());
        System.out.println("hello: " + api.hello());
    }

}
