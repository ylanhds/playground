package generon.playground.examples._12_security.server.configuration.security;

import generon.integration.cluster.server.security.GeneronServerPrincipalService;

import java.util.HashMap;
import java.util.Map;

public class ServerPrincipalServiceImpl implements GeneronServerPrincipalService {

    /**
     * Has all permissions by default with all bits enabled
     */
    private static final byte[] DEFAULT_PERMISSION_PRINCIPAL = new byte[]{0b01111111};

    private final Map<String, Byte> loginPermissions;

    public ServerPrincipalServiceImpl() {
        this.loginPermissions = new HashMap<>();
    }

    public ServerPrincipalServiceImpl(Map<String, Byte> loginPermissions) {
        this.loginPermissions = loginPermissions;
    }

    /**
     * "String data" parameter can also be used to form a principal.
     * In this example it is skipped to make it simple.
     */
    @Override
    public byte[] principal(String login, String data) {
        Byte permissions = loginPermissions.get(login);

        if (permissions == null) {
            return DEFAULT_PERMISSION_PRINCIPAL;
        }

        return new byte[]{permissions};
    }

}
