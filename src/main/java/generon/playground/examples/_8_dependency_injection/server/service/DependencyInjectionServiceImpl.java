package generon.playground.examples._8_dependency_injection.server.service;

import generon.core.annotation.Inject;
import generon.playground.examples._1_hello_world.common.service.HelloWorldService;
import generon.playground.examples._8_dependency_injection.common.service.DependencyInjectionService;
import generon.playground.examples._8_dependency_injection.server.component.EntityIdCounter;
import generon.playground.examples._8_dependency_injection.server.configuration.InjectionConfiguration;

import java.time.DayOfWeek;
import java.time.Duration;
import java.util.List;

public class DependencyInjectionServiceImpl implements DependencyInjectionService {

    @Inject(qualifier = "weekends")
    private List<DayOfWeek> weekends;

    @Inject(qualifier = "workdays")
    private List<DayOfWeek> workdays;

    @Inject
    private List<DayOfWeek> allWeekDays;

    @Inject(qualifier = "same_qualifier")
    private int primitiveInt;

    @Inject(qualifier = "same_qualifier")
    private Integer boxedInt;

    @Inject(qualifier = "same_qualifier")
    private String string;

    @Inject
    private Duration duration;

    @Inject(qualifier = "global_scope")
    private String globalScopeString; // Injected from the global scope

    @Inject
    private EntityIdCounter entityIdCounter;

    @Inject
    private InjectionConfiguration injectionConfiguration;

    @Inject
    private HelloWorldService helloWorldService;

    @Override
    public List<DayOfWeek> weekends() {
        return weekends;
    }

    @Override
    public List<DayOfWeek> workdays() {
        return workdays;
    }

    @Override
    public List<DayOfWeek> allWeekDays() {
        return allWeekDays;
    }

    @Override
    public int primitiveInt() {
        return primitiveInt;
    }

    @Override
    public Integer boxedInt() {
        return boxedInt;
    }

    @Override
    public String string() {
        return string;
    }

    @Override
    public long duration() {
        return duration.toMillis();
    }

    @Override
    public String globalScopeString() {
        return globalScopeString;
    }

    @Override
    public int nextEntityId() {
        return entityIdCounter.nextEntityId();
    }

    @Override
    public long startupTime() {
        return injectionConfiguration.startupTime();
    }

    @Override
    public String hello() {
        return helloWorldService.hello();
    }

}
