package generon.playground.examples._7_root_element_config;

import generon.client.ClientAPI;
import generon.client.TenantClientAPI;
import generon.client.root_element_config_service.RootElementConfigServiceBlockingAPI;
import generon.client.root_element_config_service.RootElementConfigServiceFactory;
import generon.playground.configuration.Example;
import generon.playground.examples._7_root_element_config.common.type.SimpleEnum;
import generon.playground.examples._7_root_element_config.common.type.object_with_root_config.ObjectWithRootConfigV1;
import generon.playground.examples._7_root_element_config.common.type.object_with_root_config.ObjectWithRootConfigV2;

import java.util.Set;

public class RootElementConfig implements Example {

    @Override
    public void run(ClientAPI clientAPI) {
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        RootElementConfigServiceFactory rootElementServiceFactory = tenantAPI.rootElementConfigServiceFactory();
        RootElementConfigServiceBlockingAPI blocking = rootElementServiceFactory.blocking();

        System.out.println(blocking.string("ASCII string"));
        System.out.println(blocking.enumType(SimpleEnum.VALUE_1));
        System.out.println(blocking.objectWithRootConfigV1(new ObjectWithRootConfigV1("value1", SimpleEnum.VALUE_1)));
        System.out.println(blocking.objectWithRootConfigV2(new ObjectWithRootConfigV2("value2", SimpleEnum.VALUE_2)));
        System.out.println(blocking.findBooksByGenre(Set.of("Fantasy", "Science")));
    }

}
