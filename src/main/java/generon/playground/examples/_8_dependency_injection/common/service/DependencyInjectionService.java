package generon.playground.examples._8_dependency_injection.common.service;

import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;

import java.time.DayOfWeek;
import java.util.List;

@Service(Constants.Id.Service.DEPENDENCY_INJECTION)
public interface DependencyInjectionService {

    @ServiceMethod(1)
    List<DayOfWeek> weekends();

    @ServiceMethod(2)
    List<DayOfWeek> workdays();

    @ServiceMethod(3)
    List<DayOfWeek> allWeekDays();

    @ServiceMethod(4)
    int primitiveInt();

    @ServiceMethod(5)
    Integer boxedInt();

    @ServiceMethod(6)
    String string();

    @ServiceMethod(7)
    long duration();

    @ServiceMethod(8)
    String globalScopeString();

    @ServiceMethod(9)
    int nextEntityId();

    @ServiceMethod(10)
    long startupTime();

    @ServiceMethod(11)
    String hello();

}
