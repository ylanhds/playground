package generon.playground;

import generon.playground.configuration.TestRunner;
import generon.playground.examples._10_event_management.EventManagement;
import generon.playground.examples._11_timer_management.TimerManagement;
import generon.playground.examples._12_security.Security;
import generon.playground.examples._13_migration_management.MigrationManagement;
import generon.playground.examples._14_direct_call.DirectCall;
import generon.playground.examples._15_multitenancy.Multitenancy;
import generon.playground.examples._1_hello_world.HelloWorld;
import generon.playground.examples._2_exception_handler.ExceptionHandler;
import generon.playground.examples._3_snapshot_field.SnapshotField;
import generon.playground.examples._4_type_support.TypeSupport;
import generon.playground.examples._5_custom_type.CustomType;
import generon.playground.examples._6_serializable_object.SerializableObject;
import generon.playground.examples._7_root_element_config.RootElementConfig;
import generon.playground.examples._8_dependency_injection.DependencyInjection;
import generon.playground.examples._9_property_management.PropertyManagement;

public class AllExamples {

    public static void main(String[] args) {
        TestRunner.runPipeline(
                TestRunner.group(
                        new HelloWorld(),
                        new TypeSupport(),
                        new CustomType(),
                        new SerializableObject(),
                        new RootElementConfig(),
                        new DependencyInjection(),
                        new PropertyManagement(),
                        new EventManagement(),
                        new TimerManagement(),
                        new MigrationManagement(),
                        new DirectCall()
                ),
                TestRunner.configurableGroup(
                        new ExceptionHandler(),
                        new Multitenancy()
                ),
                TestRunner.runnableGroup(
                        SnapshotField::run,
                        Security::run
                )
        );
    }

}
