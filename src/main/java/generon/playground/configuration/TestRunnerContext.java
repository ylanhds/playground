package generon.playground.configuration;

import generon.client.ClientAPI;
import generon.core.util.lambda.ExceptionConsumer;

import java.util.Objects;

public class TestRunnerContext {

    private ExceptionConsumer<ClientAPI> consumer;
    private TestConfigurator configurator;

    /**
     * Removes disk-related data such as archives, Raft logs, and snapshots to ensure a clean state for the next run
     */
    private boolean diskComponentCleanup = true;

    public ExceptionConsumer<ClientAPI> consumer() {
        return consumer;
    }

    public TestRunnerContext consumer(ExceptionConsumer<ClientAPI> testConsumer) {
        this.consumer = testConsumer;
        return this;
    }

    public TestConfigurator configurator() {
        return Objects
                .requireNonNullElseGet(configurator, TestConfigurator::new)
                .disableInvalidChannelErrorLogs();
    }

    public TestRunnerContext configurator(TestConfigurator testConfigurator) {
        this.configurator = testConfigurator;
        return this;
    }

    public boolean diskComponentCleanup() {
        return diskComponentCleanup;
    }

    public TestRunnerContext diskComponentCleanup(boolean clearData) {
        this.diskComponentCleanup = clearData;
        return this;
    }
}