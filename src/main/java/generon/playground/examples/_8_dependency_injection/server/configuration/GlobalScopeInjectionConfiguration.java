package generon.playground.examples._8_dependency_injection.server.configuration;

import generon.core.annotation.scan.Scope;
import generon.core.annotation.scan.configuration.Configuration;
import generon.core.annotation.scan.configuration.injection.InjectionElement;

@Configuration(scope = Scope.GLOBAL)
public class GlobalScopeInjectionConfiguration {

    @InjectionElement(qualifier = "global_scope")
    public String globalScopeString() {
        return "Global scope value";
    }

}
