package generon.playground.configuration;

import generon.client.ClientAPI;

public interface ConfigurableExample {

    void run(ClientAPI clientAPI) throws Exception;

    default TestConfigurator testConfigurator() {
        return new TestConfigurator();
    }

}
