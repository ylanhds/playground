package generon.playground.examples._3_snapshot_field.server.service;

import generon.core.annotation.BeforeSnapshot;
import generon.core.annotation.Inject;
import generon.core.annotation.SnapshotField;
import generon.playground.examples._3_snapshot_field.common.service.SnapshotFieldService;
import io.aeron.cluster.service.Cluster;
import org.agrona.collections.IntArrayList;

public class SnapshotFieldServiceImpl implements SnapshotFieldService {

    @Inject
    private Cluster cluster;

    @SnapshotField(1)
    private final IntArrayList numbers = new IntArrayList();

    @Override
    public IntArrayList addNumbers(int[] newNumbers) {
        for (int num : newNumbers) {
            numbers.add(num);
        }

        return numbers;
    }

    @BeforeSnapshot
    private void beforeSnapshot() {
        System.out.println("Before snapshot. Node id: " + cluster.memberId());
    }

}
