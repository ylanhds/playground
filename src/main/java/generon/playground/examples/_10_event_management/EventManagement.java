package generon.playground.examples._10_event_management;

import generon.client.ClientAPI;
import generon.client.EventConsumerContext;
import generon.client.EventManager;
import generon.client.TenantClientAPI;
import generon.client.event_management_service.EventManagementServiceBlockingAPI;
import generon.client.event_management_service.EventManagementServiceFactory;
import generon.client.product_event_producer.ProductEventHandler;
import generon.playground.configuration.ConsoleColor;
import generon.playground.configuration.Example;
import generon.playground.examples._10_event_management.client.event_listener.ProductDiscountEventListenerImpl;
import generon.playground.examples._10_event_management.client.event_listener.ProductEventListenerImpl;
import generon.playground.examples._10_event_management.server.CustomListenerPositionManager;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class EventManagement implements Example {

    @Override
    public void run(ClientAPI clientAPI) throws InterruptedException {
        EventManager eventManager = clientAPI.eventManager();
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        EventManagementServiceFactory eventManagementServiceFactory = tenantAPI.eventManagementServiceFactory();
        EventManagementServiceBlockingAPI api = eventManagementServiceFactory.blocking();

        // ----- Consumer 1 -----
        long productId = api.create("Name 1", 10);
        api.update(productId, "Name 2", 20);

        ConsoleColor.WHITE_BOLD.print("Connect consumer from the first CRUD event");

        long crudStreamPosition1 = 0L; // 0 means reply from beginning
        connectProductConsumer(eventManager, 1, crudStreamPosition1, 2, true);

        // ----- Consumer 2 -----
        System.out.println();
        ConsoleColor.WHITE_BOLD.print("Connect consumer from the second CRUD event");

        long crudStreamPosition2 = 96L; // 96 is a size of first event. Start from the second one
        connectProductConsumer(eventManager, 2, crudStreamPosition2, 1, true);

        // ----- Consumer 3 -----
        System.out.println();
        ConsoleColor.WHITE_BOLD.print("Connect consumer to handle different stream ids from the same event producer");

        api.stockAdd(productId, 10);
        api.stockRemove(productId, 5);

        api.discountApply(productId, 50, "123");
        api.discountRemove(productId, "123");

        api.delete(productId);

        long crudStreamPosition3 = 192L; // Skip previous two CRUD events
        int consumerId = 2; // Reuse same consumer id after it was closed
        connectProductConsumer(eventManager, consumerId, crudStreamPosition3, 5, false);
    }

    private static void connectProductConsumer(EventManager eventManager, int consumerId, long crudStreamPosition, int expectedEventAmount, boolean enablePersistLog) throws InterruptedException {
        connectConsumer(
                eventManager, consumerId, crudStreamPosition, expectedEventAmount, enablePersistLog,
                builder -> {
                    // Class level event listeners
                    ProductEventListenerImpl productEventListener = new ProductEventListenerImpl();
                    ProductDiscountEventListenerImpl productDiscountEventListener = new ProductDiscountEventListenerImpl();

                    builder.addListener(productEventListener, productDiscountEventListener);

                    // Method level event listeners. Cast is required only for cases when method signatures are the same
                    builder.addMethodListener((ProductEventHandler.ListenerContainer.StockAdded) (productId, quantity, header) -> {
                        System.out.println("Method Listener. onStockAdded: productId: " + productId + ", quantity: " + quantity + ", timestamp: " + header.timestamp);
                    });

                    builder.addMethodListener((ProductEventHandler.ListenerContainer.StockRemoved) (productId, quantity, header) -> {
                        System.out.println("Method Listener. onStockRemoved: productId: " + productId + ", quantity: " + quantity + ", timestamp: " + header.timestamp);
                    });
                }
        );
    }

    private static void connectConsumer(EventManager eventManager, int consumerId, long crudStreamPosition, int expectedEventAmount, boolean enablePersistLog, Consumer<EventConsumerContext.Builder> customizer) throws InterruptedException {
        EventConsumerContext.Builder consumerBuilder = new EventConsumerContext.Builder(consumerId);

        if (customizer != null) {
            customizer.accept(consumerBuilder);
        }

        CountDownLatch eventLatch = new CountDownLatch(expectedEventAmount);
        CustomListenerPositionManager positionManager = new CustomListenerPositionManager(crudStreamPosition, enablePersistLog, eventLatch);

        EventConsumerContext eventConsumerContext = consumerBuilder.positionManager(positionManager).build();
        eventManager.connectConsumer(eventConsumerContext);

        if (!eventLatch.await(5, TimeUnit.SECONDS)) {
            System.err.println("Event timeout");
        }

        eventManager.closeConsumer(consumerId);
    }

}
