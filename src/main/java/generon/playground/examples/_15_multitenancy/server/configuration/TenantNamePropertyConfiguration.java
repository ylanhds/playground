package generon.playground.examples._15_multitenancy.server.configuration;

import generon.core.annotation.Inject;
import generon.core.annotation.PropertyLoader;
import generon.core.annotation.scan.configuration.Configuration;
import generon.integration.cluster.internal.component.TenantMetadataComponent;

import java.util.Map;

@Configuration
public class TenantNamePropertyConfiguration {

    @Inject
    private TenantMetadataComponent tenantMetadataComponent;

    @PropertyLoader
    public Map<String, String> tenantNameProperty() {
        String propertyKey = "tenant_name";
        String propertyValue = "TenantName " + tenantMetadataComponent.tenantId();

        return Map.of(propertyKey, propertyValue);
    }

}
