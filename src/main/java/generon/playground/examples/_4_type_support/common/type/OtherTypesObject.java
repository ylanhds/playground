package generon.playground.examples._4_type_support.common.type;

import generon.core.annotation.scan.serializable.Serializable;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.StringJoiner;

@Serializable
public class OtherTypesObject {

    public BigDecimal bigDecimal;
    public BigInteger bigInteger;

    public OtherTypesObject() {
    }

    public OtherTypesObject(BigDecimal bigDecimal, BigInteger bigInteger) {
        this.bigDecimal = bigDecimal;
        this.bigInteger = bigInteger;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", OtherTypesObject.class.getSimpleName() + "[", "]")
                .add("bigDecimal=" + bigDecimal)
                .add("bigInteger=" + bigInteger)
                .toString();
    }

}
