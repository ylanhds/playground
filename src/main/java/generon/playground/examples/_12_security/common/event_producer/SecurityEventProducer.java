package generon.playground.examples._12_security.common.event_producer;

import generon.core.annotation.scan.event.EventMethod;
import generon.core.annotation.scan.event.EventProducer;
import generon.playground.configuration.Constants;

@EventProducer(value = Constants.Id.EventProducer.SECURITY, streamId = 3)
public interface SecurityEventProducer {

    @EventMethod(1)
    void event();

}

