package generon.playground.examples._5_custom_type.common.type.type_converter;

import generon.core.annotation.scan.TypeConverter;
import generon.core.objects.DecodeResult;
import generon.core.objects.type_converter.TypeConverterTemplate;
import org.agrona.DirectBuffer;
import org.agrona.MutableDirectBuffer;

import java.time.Duration;

@TypeConverter(useSizeDescriptor = true)
public class DurationTC implements TypeConverterTemplate<Duration> {

    private final DecodeResult.Object<Duration> decodeResult = new DecodeResult.Object<>();

    @Override
    public int write(Duration data, MutableDirectBuffer buffer, int offset) {
        long value = data == null ? -1L : data.toMillis();
        buffer.putLong(offset, value);

        return offset + 8;
    }

    @Override
    public DecodeResult.Object<Duration> read(DirectBuffer buffer, int offset) {
        long value = buffer.getLong(offset);
        Duration duration = value == -1L ? null : Duration.ofMillis(value);

        return decodeResult.wrap(duration, offset + 8);
    }

    @Override
    public int size(Duration data) {
        return 8;
    }
}
