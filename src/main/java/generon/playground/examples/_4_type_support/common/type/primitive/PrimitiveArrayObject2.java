package generon.playground.examples._4_type_support.common.type.primitive;

import generon.core.annotation.scan.serializable.Serializable;

import java.util.Arrays;
import java.util.StringJoiner;

@Serializable
public class PrimitiveArrayObject2 {

    public boolean[][] booleans;
    public byte[][] bytes;
    public short[][] shorts;
    public char[][] chars;
    public float[][] floats;
    public int[][] ints;
    public double[][] doubles;
    public long[][] longs;

    public PrimitiveArrayObject2() {
    }

    public PrimitiveArrayObject2(boolean[][] booleans, byte[][] bytes, short[][] shorts, char[][] chars, float[][] floats, int[][] ints, double[][] doubles, long[][] longs) {
        this.booleans = booleans;
        this.bytes = bytes;
        this.shorts = shorts;
        this.chars = chars;
        this.floats = floats;
        this.ints = ints;
        this.doubles = doubles;
        this.longs = longs;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PrimitiveArrayObject2.class.getSimpleName() + "[", "]")
                .add("booleans=" + Arrays.deepToString(booleans))
                .add("bytes=" + Arrays.deepToString(bytes))
                .add("shorts=" + Arrays.deepToString(shorts))
                .add("chars=" + Arrays.deepToString(chars))
                .add("floats=" + Arrays.deepToString(floats))
                .add("ints=" + Arrays.deepToString(ints))
                .add("doubles=" + Arrays.deepToString(doubles))
                .add("longs=" + Arrays.deepToString(longs))
                .toString();
    }

}
