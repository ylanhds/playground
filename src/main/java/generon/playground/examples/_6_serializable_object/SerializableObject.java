package generon.playground.examples._6_serializable_object;

import generon.client.ClientAPI;
import generon.client.TenantClientAPI;
import generon.client.serializable_object_service.SerializableObjectServiceBlockingAPI;
import generon.client.serializable_object_service.SerializableObjectServiceFactory;
import generon.playground.configuration.Example;
import generon.playground.examples._6_serializable_object.common.type.Address;
import generon.playground.examples._6_serializable_object.common.type.User;
import generon.playground.examples._6_serializable_object.common.type.address.AddressV1;
import generon.playground.examples._6_serializable_object.common.type.address.AddressV2;
import generon.playground.examples._6_serializable_object.common.type.user.UserV1;
import generon.playground.examples._6_serializable_object.common.type.user.UserV2;
import generon.playground.examples._6_serializable_object.common.type.user.UserV3;
import generon.playground.examples._6_serializable_object.common.type.user.UserV4;
import generon.playground.examples._6_serializable_object.common.type.user.UserV5;

public class SerializableObject implements Example {

    @Override
    public void run(ClientAPI clientAPI) {
        TenantClientAPI tenantAPI = clientAPI.tenantAPI(1);

        SerializableObjectServiceFactory serializableObjectServiceFactory = tenantAPI.serializableObjectServiceFactory();
        SerializableObjectServiceBlockingAPI api = serializableObjectServiceFactory.blocking();

        createUser_1_1(api);
        createUser_1_2(api);
        createUser_1_3(api);
        createUser_1_4(api);
        createUser_1_5(api);
        createUser_2_1(api);
        createUser_2_2(api);
        createUser_2_3(api);
        createUser_2_4(api);
        createUser_2_5(api);
        createUser_3_1(api);
    }

    private static void createUser_1_1(SerializableObjectServiceBlockingAPI api) {
        System.out.println("Method [1, 1]: " + api.createUserV1("User 1_1"));
    }

    private static void createUser_1_2(SerializableObjectServiceBlockingAPI api) {
        System.out.println("Method [1, 2]: " + api.createUserV2("User 1_2", "Full address 1_2"));
    }

    private static void createUser_1_3(SerializableObjectServiceBlockingAPI api) {
        AddressV1 address = new AddressV1("Full address 1_3");
        System.out.println("Method [1, 3]: " + api.createUserV3("User 1_3", address));
    }

    private static void createUser_1_4(SerializableObjectServiceBlockingAPI api) {
        AddressV2 address = new AddressV2(
                "Generon 1_4", "state", "city",
                "street", "postalCode", "buildingNumber"
        );

        System.out.println("Method [1, 4]: " + api.createUserV4("User 1_4", address));
    }

    private static void createUser_1_5(SerializableObjectServiceBlockingAPI api) {
        Address addressV1 = new Address();
        addressV1.version = 1;
        addressV1.fullAddress = "Full address 1_5";

        Address addressV2 = new Address();
        addressV2.version = 2;
        addressV2.country = "Generon 1_5";
        addressV2.state = "state";
        addressV2.city = "city";
        addressV2.street = "street";
        addressV2.postalCode = "postalCode";
        addressV2.buildingNumber = "buildingNumber";

        System.out.println("Method [1, 5]. Address V1: " + api.createUserV5("User 1_5 1", addressV1));
        System.out.println("Method [1, 5]. Address V2: " + api.createUserV5("User 1_5 2", addressV2));
    }

//    ---

    private static void createUser_2_1(SerializableObjectServiceBlockingAPI api) {
        UserV1 user = new UserV1(1001L, "User 2_1");
        System.out.println("Method [2, 1]: " + api.createUserV1(user));
    }

    private static void createUser_2_2(SerializableObjectServiceBlockingAPI api) {
        UserV2 user = new UserV2(1002L, "User 2_1", "Full address 2_2");
        System.out.println("Method [2, 2]: " + api.createUserV2(user));
    }

    private static void createUser_2_3(SerializableObjectServiceBlockingAPI api) {
        AddressV1 address = new AddressV1("Full address 2_3");
        UserV3 user = new UserV3(1003L, "User 2_3", address);

        System.out.println("Method [2, 3]: " + api.createUserV3(user));
    }

    private static void createUser_2_4(SerializableObjectServiceBlockingAPI api) {
        AddressV2 address = new AddressV2(
                "Generon 2_4", "state", "city",
                "street", "postalCode", "buildingNumber"
        );
        UserV4 user = new UserV4(1004L, "User 2_4", address);

        System.out.println("Method [2, 4]: " + api.createUserV4(user));
    }

    private static void createUser_2_5(SerializableObjectServiceBlockingAPI api) {
        Address addressV1 = new Address();
        addressV1.version = 1;
        addressV1.fullAddress = "Full address 2_5";

        Address addressV2 = new Address();
        addressV2.version = 2;
        addressV2.country = "Generon 2_5";
        addressV2.state = "state";
        addressV2.city = "city";
        addressV2.street = "street";
        addressV2.postalCode = "postalCode";
        addressV2.buildingNumber = "buildingNumber";

        UserV5 user1 = new UserV5(1001L, "User 2_5 1", addressV1);
        UserV5 user2 = new UserV5(1002L, "User 2_5 2", addressV2);

        System.out.println("Method [2, 5]. Address V1: " + api.createUserV5(user1));
        System.out.println("Method [2, 5]. Address V2: " + api.createUserV5(user2));
    }

    private static void createUser_3_1(SerializableObjectServiceBlockingAPI api) {
        User userV1 = new User();
        userV1.version = 1;
        userV1.id = 1003L;
        userV1.name = "User 3_1 V1";

        User userV2 = new User();
        userV2.version = 2;
        userV2.id = 1004L;
        userV2.name = "User 3_1 V2";
        userV2.fullAddress = "Full address 3_1 1";

        User userV3 = new User();
        userV3.id = 1005L;
        userV3.version = 3;
        userV3.name = "User 3_1 V3";
        userV3.address = new Address();
        userV3.address.version = 1; // We can skip this line. Version 1 is determined by user version 3
        userV3.address.fullAddress = "Full address 3_1 2";

        User userV4 = new User();
        userV4.id = 1006L;
        userV4.version = 4;
        userV4.name = "User 3_1 V4";
        userV4.address = new Address();
        userV4.address.version = 2; // We can skip this line. Version 2 is determined by user version 4
        userV4.address.country = "Generon 2_5";
        userV4.address.state = "state";
        userV4.address.city = "city";
        userV4.address.street = "street";
        userV4.address.postalCode = "postalCode";
        userV4.address.buildingNumber = "buildingNumber";

        User userV5 = new User();
        userV5.version = 5;
        userV5.id = 1007L;
        userV5.name = "User 3_1 V5";
        userV5.address = null;

        System.out.println("Method [3, 1]. UserV1: " + api.createUser(userV1));
        System.out.println("Method [3, 1]. UserV2: " + api.createUser(userV2));
        System.out.println("Method [3, 1]. UserV3: " + api.createUser(userV3));
        System.out.println("Method [3, 1]. UserV4: " + api.createUser(userV4));
        System.out.println("Method [3, 1]. UserV5: " + api.createUser(userV5));
    }

}
