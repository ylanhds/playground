package generon.playground.examples._13_migration_management.common.service;

import generon.core.annotation.scan.serializable.migration.object.ObjectVersion;
import generon.core.annotation.scan.service.Service;
import generon.core.annotation.scan.service.ServiceMethod;
import generon.playground.configuration.Constants;
import generon.playground.examples._13_migration_management.common.type.SimpleEntity;

@Service(Constants.Id.Service.MIGRATION_MANAGEMENT)
public interface MigrationManagementService {

    @ServiceMethod(1)
    int createEntity(String name);

    @ServiceMethod(2)
    @ObjectVersion(1)
    SimpleEntity getEntity(int id);

}
