package generon.playground.examples._5_custom_type.common.type.buffer_object;

import generon.core.objects.buffer.BufferObject;
import generon.utils.ByteConstants;
import org.agrona.DirectBuffer;
import org.agrona.MutableDirectBuffer;

import java.time.Instant;

public class InstantBO implements BufferObject<InstantBO.Reader, InstantBO.Writer> {

    private Reader reader;
    private Writer writer;

    @Override
    public Reader reader() {
        return reader == null ? (reader = new Reader(writer)) : reader;
    }

    @Override
    public Writer writer() {
        return writer == null ? (writer = new Writer(reader)) : writer;
    }

    public static class Reader implements BufferObject.Reader {

        public boolean isNull;
        public long timestamp;

        public Reader(Writer writer) {
            if (writer == null) {
                return;
            }

            this.isNull = writer.isNull;
            this.timestamp = writer.timestamp;
        }

        @Override
        public int read(DirectBuffer buffer, int offset) {
            if (buffer.getByte(offset++) == ByteConstants.B0) {
                this.isNull = true;
                return offset;
            }

            this.isNull = false;

            this.timestamp = buffer.getLong(offset);
            offset += 8;

            return offset;
        }

        public Instant asInstant() {
            if (isNull) {
                return null;
            }

            return Instant.ofEpochMilli(timestamp);
        }

    }

    public static class Writer implements BufferObject.SizedWriter {

        private boolean isNull;
        private long timestamp;

        public Writer(Reader reader) {
            if (reader == null) {
                return;
            }

            this.isNull = reader.isNull;
            this.timestamp = reader.timestamp;
        }

        public Writer wrapAsNull() {
            this.isNull = true;
            this.timestamp = -1L;

            return this;
        }

        public Writer wrap(long timestamp) {
            this.isNull = false;
            this.timestamp = timestamp;

            return this;
        }

        public Writer wrap(Instant instant) {
            if (instant == null) {
                this.isNull = true;
                this.timestamp = -1L;

                return this;
            }

            this.isNull = false;
            this.timestamp = instant.toEpochMilli();

            return this;
        }

        @Override
        public int write(MutableDirectBuffer buffer, int offset) {
            if (isNull) {
                buffer.putByte(offset++, ByteConstants.B0);
                return offset;
            }

            buffer.putByte(offset++, ByteConstants.B1);
            buffer.putLong(offset, this.timestamp);
            offset += 8;

            return offset;
        }

        @Override
        public int messageLength() {
            if (isNull) {
                return Byte.BYTES; // null header
            }

            return 9; // Byte.BYTES + Long.BYTES
        }

    }

}
