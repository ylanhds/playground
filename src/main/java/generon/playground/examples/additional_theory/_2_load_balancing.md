Recommended to read after `DirectCall`

### General idea

To have fault-tolerant and high-available system we need a cluster with multiple nodes or application instances which
are working together using some consensus algorithms. In `Raft`, we have `leader` and `follower`. In other systems it
may be called `master` and `slave` or `primary` and `secondary` etc. Generon is based on Aeron Cluster which is using
Raft. Leader in all systems does more work comparing to follower, and it is a potential bottleneck of the system,
meaning cluster operates with a speed of a leader. Cluster is fast when majority is fast. But when leader is slow entire
cluster is slow. Sometimes leader can do much more work than followers, when clients use leader as a source. This
difference can be in ranges like 50%-200%, which is a relatively a lot. Scale of it can vary and depends on the type of
the system. Such a big difference can be observed when leader is responsible for all connections, event subscriptions,
especially when there is disk involved. It is also responsible for managing replications, sending responses to the
clients using egress - responses for API calls via SDK. There is a way to move part of work from `leader`to `follower`
which will bring balanced load and overall better cluster performance. The only thing which follower can help with
is `read-only` operations like API calls and event listeners. Most systems have proportionally more `read-only` requests
than write ones. Such operations we can forward to followers, but `write operations` or operations which need to update
a state we `must forward to leader` as usual. Write operations should go through the `Raft Log`, be replicated and
processed by all nodes. For read operations we can avoid these steps and process it only by one node without any impact.
For example, we have 3 nodes - one leader and two followers. If we need to take a user profile information which is
a `read` operation we can use one of the followers directly. Leader and follower both have `eventual consistency` and
work `asynchronously`, meaning that conceptually we are not losing `consistency` by using follower, we may only lose
freshness of data by matter or microseconds. When we need to update profile we can not use direct call to follower
because all instances should receive and process this command to end up with same state. Even if we are using leader for
direct read-only requests it still have performance benefit, because command will not go to the `Raft Log` which use
disk and does replication in case multi node cluster setup. It will be RAM only operation without any side effect work.
Having such load balancing feature significantly increases overall cluster performance. Multiple clients can connect to
different followers and make load more balanced.

### Solution

Generon has built in load balance management system for `read-only` operations like direct API calls and event
listening. In single node cluster setup it obviously will use only 1 node which is leader. But when there are multiple
nodes in the cluster it will not use leader and use followers only.

When connected `node goes down` client will `use another available node`. When leader is dropped, cluster will do
reelection and one of the followers will become a leader. When client was connected to a follower which became leader,
it will disconnect from it to `always use followers`. When connected follower goes down for any reason it will use
another available node and if there are no available followers it will try to reconnect infinitely.

Direct `read-only` API calls will `pick a node by round-robin` on each request, if it is not available next one will
be used. In some edge cases it `can end up with timeout exception`. For API calls node will use RAM only without any
additional work, which gives the best performance possible.

When new event consumer is connected, all subscriptions will go to the node taken selected by round-robin. Client can
connect `multiple independent consumers` with `own tracking positions` per streamId. When node is disconnected all
consumers sitting on that node will be `safely moved` to another available node, or it will stay in detached state until
at least one node will become available again. Event consumer can use `RAM or disk` depending on what client needs and
how client performs. Usually it uses RAM when consumer is fast enough to follow live buffer. But when client needs older
data or when it is slow enough to read from live buffer, which already does not have needed event, it will use disk.
After it is caught up it will switch to live and not use disk. By default, archive of the node supports 20 disk
connections and can be configured using `archiveContext.maxConcurrentReplays(N)`. 1 connection means 1 streamId
subscription. Single consumer can have multiple streams depending on how event producers are configured.

RAM event subscriptions are not supported yet and will be available soon.