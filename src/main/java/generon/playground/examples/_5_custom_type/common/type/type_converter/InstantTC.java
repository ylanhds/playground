package generon.playground.examples._5_custom_type.common.type.type_converter;

import generon.core.annotation.scan.TypeConverter;
import generon.core.objects.DecodeResult;
import generon.core.objects.type_converter.TypeConverterTemplate;
import generon.utils.ByteConstants;
import org.agrona.DirectBuffer;
import org.agrona.MutableDirectBuffer;

import java.time.Instant;

@TypeConverter(useSizeDescriptor = true)
public class InstantTC implements TypeConverterTemplate<Instant> {

    private final DecodeResult.Object<Instant> decodeResult = new DecodeResult.Object<>();

    @Override
    public int write(Instant data, MutableDirectBuffer buffer, int offset) {
        if (data == null) {
            buffer.putByte(offset++, ByteConstants.B0);
            return offset;
        }

        buffer.putByte(offset++, ByteConstants.B1);
        buffer.putLong(offset, data.toEpochMilli());
        offset += 8;

        return offset;
    }

    @Override
    public DecodeResult.Object<Instant> read(DirectBuffer buffer, int offset) {
        boolean isNull = buffer.getByte(offset++) == ByteConstants.B0;

        if (isNull) {
            return decodeResult.wrap(null, offset);
        }

        long timestamp = buffer.getLong(offset);
        offset += 8;

        return decodeResult.wrap(Instant.ofEpochMilli(timestamp), offset);
    }

    @Override
    public int size(Instant data) {
        if (data == null) {
            return Byte.BYTES; // null header
        }

        return 9; // Byte.BYTES + Long.BYTES
    }
}